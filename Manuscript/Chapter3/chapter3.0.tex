The final decades of the twentieth century witnessed a major revolution in solid-state and molecular physics, as the introduction of sophisticated exchange-correlation models \cite{ParrYang} propelled DFT from qualitative to quantitative usefulness.
The apotheosis of this development was probably the award of the 1998 Nobel Prize for Chemistry to Walter Kohn and John Pople but its origins can be traced to the prescient efforts by Thomas \cite{Thomas27}, Fermi \cite{Fermi26} and Dirac \cite{Dirac30}, more than 70 years earlier, to understand the behaviour of ensembles of electrons without explicitly constructing their full wave functions. 
These days, DFT so dominates the popular perception of molecular orbital calculations that many non-specialists now regard the two as synonymous.

In principle, the cornerstone of modern DFT is the HK theorem \cite{Hohenberg64} but, in practice, it rests largely on the presumed similarity between the electronic behaviour in a real system and that in the hypothetical ``infinite'' uniform electron gas (IUEG) or jellium \cite{Fermi26, Thomas27, Dirac30, Wigner34, Macke50, GellMann57, Onsager66, Stern73, Rajagopal77, Isihara80, Hoffman92, Seidl04, Sun10, 2DEG11, 3DEG11, WIREs16, Vignale}.
In 1965, Kohn and Sham \cite{Kohn65} showed that the knowledge of an analytical parametrisation of the IUEG correlation energy allows one to perform approximate calculations for atoms, molecules and solids.
The idea --- the local-density approximation (LDA) --- is attractively simple:  if we know the properties of jellium, we can understand the electron cloud in a molecule by dividing it into tiny chunks of density and treating each as a piece of jellium.

The good news is that the properties of jellium are known from DMC calculations \cite{Ceperley80, Tanatar89, Kwon93, Ortiz94, Rapisarda96, Kwon98, Ortiz99, Attaccalite02, Zong02, Drummond09a, Drummond09b}.
Such calculations are possible because jellium is characterised by just a \emph{single} parameter $\rho$, the electron density.

This spurred the development of a wide variety of spin-density correlation functionals (VWN \cite{VWN80}, PZ \cite{PZ81}, PW92 \cite{PW92}, etc), each of which requires information on the high- and low-density regimes of the spin-polarised IUEG, and are parametrised using numerical results from QMC calculations \cite{Ceperley78, Ceperley80}, together with analytic perturbative results.

The bad news is that jellium has an infinite number of electrons in an infinite volume and this unboundedness renders it, in some respects, a poor model for the electrons in molecules.  
Indeed, the simple LDA described above predicts bond energies that are much too large and this led many chemists in the 70's to dismiss DFT as a quantitatively worthless theory.

Most of the progress since these days has resulted from concocting ingenious corrections for jellium's deficiencies (GGAs, MGGAs, HGGAs, etc).

However, notwithstanding the impressive progress since the 70's, modern DFT approximations still exhibit fundamental deficiencies in large systems \cite{Curtiss00}, conjugated molecules \cite{Woodcock02}, charge-transfer excited states \cite{Dreuw04}, dispersion-stabilised systems \cite{Wodrich06}, systems with fractional spin or charge \cite{Yang08}, isodesmic reactions \cite{Brittain09}, and elsewhere.  
Because DFT is in principle an exact theory, many of these problems can be traced ultimately to the use of jellium as a reference system and the \textit{ad hoc} corrections that its use subsequently necessitates. 

