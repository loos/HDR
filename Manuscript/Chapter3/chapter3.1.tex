%****************************************************************
\section{Uniform electron gases}
%****************************************************************
In the previous chapter, we considered the behaviour of electrons that are confined to the surface of a sphere.  
This work yielded a number of unexpected discoveries \cite{TEOAS09, EcLimit09, QuasiExact09, Concentric10, Hook10, EcProof10, ExSpherium10, Frontiers10, Glomium11} but the one of relevance here is that such systems provide a beautiful new family of UEGs.
These finite UEGs (FUEGs) have been thoroughly studied in Ref.~\cite{Glomium11}.
Here, we only report their main characteristics \cite{UEGs12}.

\begin{table}
	\centering
	\caption{
	\label{tab:Ylm}
	The lowest free-particle orbitals on a 2-sphere}
	\begin{tabular}{lccc}
		\hline
		Name		&	$l$	&	$m$	&	$\sqrt{4\pi} \,Y_{lm}(\theta,\phi)$				\\
		\hline
		$s$		&	$0$	&	$0$		&	$1$							\\
		\hline
		$p_0$		&	$1$	&	$0$		&	$3^{1/2} \cos\theta$					\\
		$p_{+1}$	&	$1$	&	$+1$		&	$(3/2)^{1/2} \sin\theta \exp(+i\phi)$			\\
		$p_{-1}$	&	$1$	&	$-1$		&	$(3/2)^{1/2} \sin\theta \exp(-i\phi)$			\\
		\hline
		$d_0$		&	$2$	&	$0$		&	$(5/4)^{1/2} (3\cos^2\theta-1)$				\\
		$d_{+1}$	&	$2$	&	$+1$		&	$(15/2)^{1/2} \sin\theta \cos\theta \exp(+i\phi)$	\\
		$d_{-1}$	&	$2$	&	$-1$		&	$(15/2)^{1/2} \sin\theta \cos\theta \exp(-i\phi)$	\\
		$d_{+2}$	&	$2$	&	$+2$		&	$(15/8)^{1/2} \sin^2\theta \exp(+2i\phi)$		\\
		$d_{-2}$	&	$2$	&	$-2$		&	$(15/8)^{1/2} \sin^2\theta \exp(-2i\phi)$		\\
		\hline
	\end{tabular}
\end{table}

\begin{table}
	\centering
	\caption{Number of electrons in $L$-spherium and $L$-glomium atoms}
	\label{tab:fullshell}
	\begin{tabular}{ccccccccc}
		\hline
		$L$		&	0	&	1	&	2	&	3	&	4		&	5		&	6		&	7	\\
		\hline
		$L$-spherium	&	2	&	8	&	18	&	32	&	50		&	72		&	98		&	128	\\
		$L$-glomium	&	2	&	10	&	28	&	60	&	110		&	182		&	280		&	408	\\
		\hline
	\end{tabular}
\end{table}

\begin{table}
	\centering
	\caption{The lowest free-particle orbitals on a glome (\textit{i.e.}~a 3-sphere)}
	\label{tab:Ylmn}
	\begin{tabular}{lcccc}
		\hline
		Name		&	$l$	&	$m$	&	$n$		&	$\pi\, Y_{lmn}(\chi,\theta,\phi)$			\\
		\hline
		$1s$		&	$0$	&	$0$	&	$0$		&	$2^{-1/2}$						\\
		\hline
		$2s$		&	$1$	&	$0$	&	$0$		&	$2^{1/2} \cos\chi$					\\
		$2p_0$		&	$1$	&	$1$	&	$0$		&	$2^{1/2} \sin\chi \cos\theta$				\\
		$2p_{+1}$	&	$1$	&	$1$	&	$+1$		&	$\sin\chi \sin\theta \exp(+i\phi)$			\\
		$2p_{-1}$	&	$1$	&	$1$	&	$-1$		&	$\sin\chi \sin\theta \exp(-i\phi)$			\\
		\hline
		$3s$		&	$2$	&	$0$	&	$0$		&	$2^{-1/2} (4\cos^2\chi-1)$				\\
		$3p_0$		&	$2$	&	$1$	&	$0$		&	$12^{1/2}\sin\chi \cos\chi \cos\theta$			\\
		$3p_{+1}$	&	$2$	&	$1$	&	$+1$		&	$6^{1/2}\sin\chi \cos\chi \sin\theta \exp(+i\phi)$	\\
		$3p_{-1}$	&	$2$	&	$1$	&	$-1$		&	$6^{1/2}\sin\chi \cos\chi \sin\theta \exp(-i\phi)$	\\
		$3d_0$		&	$2$	&	$2$	&	$0$		&	$\sin^2\chi \ (3\cos^2\theta-1)$			\\
		$3d_{+1}$	&	$2$	&	$2$	&	$+1$		&	$6^{1/2}\sin^2\chi \sin\theta \cos\theta \exp(+i\phi)$	\\
		$3d_{-1}$	&	$2$	&	$2$	&	$-1$		&	$6^{1/2}\sin^2\chi \sin\theta \cos\theta \exp(-i\phi)$	\\
		$3d_{+2}$	&	$2$	&	$2$	&	$+2$		&	$(3/2)^{1/2}\sin^2\chi \sin^2\theta \exp(+2i\phi)$	\\
		$3d_{-2}$	&	$2$	&	$2$	&	$-2$		&	$(3/2)^{1/2}\sin^2\chi \sin^2\theta \exp(-2i\phi)$	\\
		\hline
	\end{tabular}
\end{table}

%-----------------------------------------------------------------------
\subsection{Spherium atoms}
%-----------------------------------------------------------------------
The surface of a three-dimensional ball is called a 2-sphere (for it is two-dimensional) and its free-particle orbitals (Table \ref{tab:Ylm}) are the spherical harmonics $Y_{lm}(\theta,\phi)$.  
It is known that
\begin{equation}
	\sum_{m=-l}^l \abs{Y_{lm}(\theta,\phi) }^2 = \frac{2l+1}{4\pi},
\end{equation}
and doubly occupying all the orbitals with $0 \le l \le L$ thus yields a UEG.  
We call this system $L$-spherium and will compare it to \emph{two}-dimensional jellium \cite{Vignale}.

The number of electrons (Table \ref{tab:fullshell}) in $L$-spherium is
\begin{equation}
	n = 2(L+1)^2,
\end{equation}
the volume of a 2-sphere is $V = 4\pi R^2$ and, therefore,
\begin{equation} \label{eq:rho2d}
	\rho = \frac{(L+1)^2}{2\pi R^2}.
\end{equation}

%---------------------------------------------------------------------
\subsection{Glomium atoms}
%---------------------------------------------------------------------
The surface of a four-dimensional ball is a 3-sphere (or ``glome'') and its free-particle orbitals (Table \ref{tab:Ylmn}) are the hyperspherical harmonics $Y_{lmn}(\chi,\theta,\phi)$.  
It is known \cite{Avery} that
\begin{equation}
	\sum_{m=0}^l \sum_{n=-m}^m \abs{Y_{lmn}(\chi,\theta,\phi) }^2 = \frac{(l+1)^2}{2\pi^2},
\end{equation}
and doubly occupying all the orbitals with $0 \le l \le L$ thus yields a UEG.
We call this system $L$-glomium and will compare it to \emph{three}-dimensional jellium \cite{Vignale}.

The number of electrons (Table \ref{tab:fullshell}) in $L$-glomium is
\begin{equation}
	n = (L+1)(L+2)(2L+3)/3,
\end{equation}
the volume of a 3-sphere is $V = 2\pi^2 R^3$ and, therefore,
\begin{equation}
	\rho = \frac{(L+1)(L+2)(2L+3)}{6\pi^2 R^3}.
\end{equation}

%============================================
\subsection{The non-uniqueness problem}
%============================================
The deeply disturbing aspect of jellium-based DFT models --- and the launching-pad for the remainder of this chapter --- is the countercultural claim, that
\begin{quote}
	\textit{``The uniform electron gas with density $\rho$ is not unique.''}
\end{quote}
Though it may seem heretical to someone who has worked with jellium for many years, or to someone who suspects that the claim violates the HK theorem, we claim that two $D$-dimensional UEGs with the same density parameter $\rho$ may have different energies.  
To illustrate this, we now show that density functionals \cite{Dirac30, PW92, Attaccalite02} which are exact for jellium are wrong for 0-spherium and 0-glomium.

The energy contributions for 0-spherium and 0-glomium are easy to find.  
There is no external potential, so $\Ene = 0$.  
The density $\rho(\br)$ is constant, so the KS orbital $\psi(\br) = \sqrt{\rho(\br)}$ is constant, and $\Ts=0$.  
The Hartree energy is the self-repulsion of a uniform spherical shell of charge of radius $R$ and one finds \cite{EcLimit09}
\begin{equation}
	J = \frac{\Gamma(D-1)}{\Gamma(D-1/2)} \frac{\Gamma(D/2+1/2)}{ \Gamma(D/2)} \frac{1}{R},
\end{equation}
where $\Gamma(x)$ is the Gamma function \cite{NISTbook}.
The exchange energy is predicted to be \cite{Glomium11}
\begin{equation}
	\Ex = - \frac{2D}{(D^2-1)\pi R} \left(\frac{D!}{2} \right)^{1/D},
\end{equation}
and the correlation energy is predicted by conventional jellium-based functionals \cite{Dirac30, PW92, Attaccalite02}.

Applying these formulae to the exactly solvable states of 0-spherium and 0-glomium considered in the previous chapter yields the results in the right half of Table \ref{tab:properties}.  
In all cases, the KS-DFT energies are too high by 10 -- 20\%, indicating that the correlation functional that is exact for the UEG in jellium grossly underestimates the correlation energy of the UEGs in 0-spherium and 0-glomium.

\begin{table*}
	\centering
	\footnotesize
	\caption{Exact and KS reduced energies of the ground states of 0-spherium and 0-glomium for various eigenradii $R$}
	\label{tab:properties}
	\begin{tabular}{ccccclcccccc}
	\hline
			&		&			\mc{3}{c}{Exact}			&				\mc{6}{c}{Jellium-based KS DFT}				&	Error	\\
					\cline{3-5}	\cline{6-11}	\cline{12-12}
			&	$2R$	&	$T$		&	$\Eee$		&  $E$		&	$\Ts$	&$\Ene$		&	$J$		&	$-\Ex$		& 	$-\Ec$	&  $\EKS$	&	$\EKS-E$	\\
	\hline
	0-spherium	&  $\sqrt{3}$	&	0.051982	&	0.448018	&	1/2	&	0	&	0	&	1.154701	&	0.490070	&	0.1028	&	0.562	&	0.062	\\
			&  $\sqrt{28}$	&	0.018594	&	0.124263	&	1/7	&	0	&	0	&	0.377964	&	0.160413	&	0.0593	&	0.158	&	0.015	\\
	\hline
	0-glomium	&  $\sqrt{10}$	&	0.014213	&	0.235787	&	1/4	&	0	&	0	&	0.536845	&	0.217762	&	0.0437	&	0.275	&	0.025	\\
			&  $\sqrt{66}$	&	0.007772	&	0.083137	&  1/11	&	0	&	0	&	0.208967	&	0.084764	&	0.0270	&	0.097	&	0.006	\\
	\hline
	\end{tabular}
\end{table*}

We know that it is possible for two UEGs to have the same density $\rho$ but different reduced energies $E$.  
But how can this be, given that the probability of finding an electron in a given volume is identical in the two systems?  
The key insight is that the probability of finding \emph{two} electrons in that volume is different.

This is illustrated in Fig.~\ref{fig:PuR}, which compare the probability distributions of the interelectronic distance $u$ \cite{Coulson61, GoriGiorgi04, Concentric10} in various two-dimensional UEGs.
These reveal that, although similar for $u \approx 0$ (because of the Kato cusp condition \cite{Kato57}), the specific Coulomb holes (i.e.~the holes per unit volume \cite{Overview03}) in two gases with the same one-electron density $\rho$ can be strikingly different.  
In each case, the jellium hole is both deeper and wider than the corresponding spherium hole, indicating that the jellium electrons exclude one another more strongly, and one is much less likely to find two electrons in a given small volume of jellium than in the same volume of spherium.

\begin{figure}
	\centering
	\includegraphics[width=0.45\textwidth]{../Chapter3/fig/PuR1}
	\includegraphics[width=0.45\textwidth]{../Chapter3/fig/PuR2}
	\caption{
	\label{fig:PuR}
	Specific Coulomb holes for 0-spherium (dotted) with $R=\sqrt{3}/2$ (left) and $R=\sqrt{7}$ (right), and 2D jellium (solid).  
	Both are uniform gases with $\rho = 2/(3\pi)$ (left) and $\rho = 1/(14\pi)$ (right)}
\end{figure}


We conclude from these comparisons that (at least) two parameters are required to characterise a UEG.
Although the parameter choice is not unique, we believe that the first should be a one-electron quantity, such as the density $\rho$ and the second should be a two-electron quantity.
A possible choice of a two-electron local variable will be presented in the next section.

