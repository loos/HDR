In this memoir, I have presented succinctly some of the research projects we have been pursuing in the last ten years.
Moreover, I have discussed two of our current research topics in the final chapter.
As concluding remarks, I would like to talk further about various research projects we would like to work on in the years to come.
This is a non-exhaustive list in no particular order but I hope it will give to the reader a feeling about what we are trying to achieve.

%************************************************************************
\paragraph{What is the exact Green function?}
%************************************************************************
As mentioned earlier in the memoir, exactly solvable models are particularly valuable for testing theoretical approaches.
Here, we would like to use the electrons-on-a-sphere model presented above to unveil the form of the exact Green function \cite{Berger14}.
Green function-based methods allows an explicit incorporation of the electronic correlation effects through a summation of Feynman diagrams.
Important properties such as total energies, ionisation potentials, electron affinities as well as photo-emission spectra can be obtained directly from the Green function.	
A particularly successful variant of these methods in electronic-structure calculations is the so-called GW approximation, which consists in evaluating the self-energy $\Sigma$ starting from the Green function $G$ using a sequence of self-consistent steps (see Fig.~\ref{fig:pentagon}).

Thanks to this toy system, our preliminary results show that we might be able to compute self-consistently $\Sigma$.
This will help us understand approximation such as G$_0$W$_0$ where one eschews the iterative process.
More importantly, we might be able to obtain the closed-form expression of the vertex correction $\Gamma$ --- a quantity hardly accessible in real systems --- and complete the entire five-step  self-consistent process (see Fig.~\ref{fig:pentagon}).
This could shed lights on how to approximate wisely the vertex function in real systems.

\begin{figure}
	\centering
	\includegraphics[width=0.4\linewidth]{../Conclusion/fig/pentagon}
	\caption{
	\label{fig:pentagon}
	Hedin's pentagon \cite{Hedin65}.}
\end{figure}

%************************************************************************
\paragraph{GLDA correlation functional}
%************************************************************************
In Sec.~\ref{sec:ExGLDA}, we have presented an exchange functional based on FUEGs.
In order to create the associated correlation functional, one would need to compute accurate energies of FUEGs for various number of electrons and densities.
One of the method of choice to achieve this would probably be DMC.
Sadly, DMC on a curved manifold (like a $D$-sphere) is not as easy as one would have thought.

What are the modifications required to the VMC and DMC algorithms to perform calculations of electrons in curved manifolds?
While the modifications required for the VMC algorithm are fairly straightforward \cite{SGF14, Nodes15, LowGlo15}, the modifications one has to do in the DMC algorithm are much more subtle. 
DMC on a curved manifold has received very little attention in the past. 
To the best of our knowledge, the only work related to this has been done by Ortiz and coworkers to calculate the energy of composite fermions on the Haldane sphere \cite{Melik97}.
On a curved manifold, when moving the electrons, one has to make sure that the move keeps the electrons on the surface of the sphere. 

In VMC, the only required modification is the Metropolis acceptance probability \cite{Nodes15, LowGlo15}.
On a curved manifold, the DMC algorithm requires modifications in the diffusion and drift processes.
The branching process is not affected by the curved nature of the manifold.
The major difference appears in the calculation of the short-time approximation of the Green function

This ``curved'' DMC method could be efficiently implemented in the local QMC software package (QMC=Chem) developed by Scemama, Caffarel and coworkers \cite{Scemama13b}.
This would allow to perform DMC calculations and obtain the near-exact energies of FUEGs required to build the three-dimensional version of the GLDA correlation functional, similarly to what we have done in the one-dimensional case \cite{gLDA14, Wirium14}.

%************************************************************************
\paragraph{Symmetry-broken LDA}
%************************************************************************
Within DFT, the LDA correlation functional is typically built by fitting the difference between the near-exact and HF energies of the UEG, together with analytic perturbative results from the high- and low-density regimes. 
Near-exact energies are obtained by performing accurate DMC calculations, while HF energies are usually assumed to be the Fermi fluid HF energy. 
However, it has been known since the seminal work of Overhauser \cite{Overhauser59, Overhauser62} that one can obtain lower, symmetry-broken (SB) HF energies at any density \cite{Trail03, Delyon08, Bernu08, Bernu11, Baguet13, Baguet14, Delyon15}.
Recently, we have computed the SBHF energies of the one-dimensional UEG \cite{1DEG13, ESWC17} and constructed a SB version of the LDA (SBLDA) from these results \cite{SBLDA16}.
The newly designed functional, which we have named SBLDA, has shown to surpass the performance of its LDA parent in providing better estimates of the correlation energy in one-dimensional systems \cite{gLDA14, Wirium14, 1DChem15, Ball15, Leglag17}.
Based on the same methodology, we would like to design of new exchange and correlation functionals for two- and three-dimensional systems for which SBHF calculations have already been performed \cite{Trail03, Bernu11, Baguet13, Baguet14}.

%************************************************************************
\paragraph{Resolution of geminals}
%************************************************************************
The main idea behind the ``resolution of geminals'' (RG) is a generalisation of the resolution of the identity (RI).
To explain what we mean by this, let us state the RI identity in its two-electron version, i.e.
\begin{equation}
\label{eq:2eRI} 
	\delta(\ree) = \sum_{\mu}^\infty \dyad{\chi_\mu(\br_1)}{\chi_\mu(\br_2)},
\end{equation}
where $\delta(r)$ is the Dirac delta function and the one-electron basis set $\chi_\mu(\br)$ is formally complete.
In other words, we have just ``resolved'' the Dirac delta function, i.e. we wrote a two-electron function as a sum of products of one-electron functions.
One can show that most of the usual two-electron operators used in quantum chemistry can be written in a similar form.

For example, one can write a Gaussian geminal, using a Gauss-Hermite quadrature, as \cite{Limpanuparb11b}
\begin{equation}
	G_{12} = \sum_{n \ell m} \dyad{\phi_{n \ell m}(\br_1)}{\phi_{n \ell m}(\br_2)},
\end{equation}
with
\begin{equation}
	\phi_{n \ell m}(\br) = \sqrt{8 \pi^{1/2}b_n}\,\beta_n\,j_{\ell} (2\sqrt{\lambda}\,\beta_n\,r) Y_{\ell m}(\bm{r}),
\end{equation}
where $\beta_n$ and $b_n$ are the (positive) Hermite roots and weights, and $j_n(r)$ is a spherical Bessel function of the first kind \cite{NISTbook}.
A similar expression can be found for the long-range Coulomb operator, the Slater geminal and many others.

In this way, as one inserts the RI ``in-between'' two operators, one can now resolve operators!
This is illustrated diagrammatically in Fig.~\ref{fig:RIvsRG} for some of the three-electron integrals involved in explicitly-correlated methods.
This procedure generates new integrals as one has to deal with spherical Bessel functions now. 
However, these are only one- and two-electron integrals.

%%% FIGURE 1 %%%
\begin{figure*}
	\centering
	\begin{minipage}[b]{0.3\textwidth}
		\includegraphics[width=0.8\textwidth]{../Conclusion/fig/fig1a}
		\scriptsize $I_{ijk}^{lmn} \overset{\text{RI}} \approx  \sum_{\mu} G_{i\mu}^{jm} F_{\mu l}^{kn}$
	\end{minipage}
	\begin{minipage}[b]{0.3\textwidth}
		\includegraphics[width=0.8\textwidth]{../Conclusion/fig/fig1b}
		\scriptsize $I_{ijk}^{lmn} = \left< ijk  \left| r_{12}^{-1}\,f_{13} \right| lmn \right>$
	\end{minipage}
	\begin{minipage}[b]{0.3\textwidth}
		\includegraphics[width=0.8\textwidth]{../Conclusion/fig/fig1c}
		\scriptsize $I_{ijk}^{lmn} \overset{\text{RG}} \approx \sum_{\mu}G_{il,\mu}^{jm}  S_{kn,\mu} $
	\end{minipage}
%	
	\begin{minipage}[b]{0.3\textwidth}
		\includegraphics[width=0.8\textwidth]{../Conclusion/fig/fig2a}
		\scriptsize $J_{ijk}^{lmn} \overset{\text{RI}} \approx  \sum_{\mu} F_{i\mu}^{jm} F_{\mu l}^{kn}$
	\end{minipage}
	\begin{minipage}[b]{0.3\textwidth}
		\includegraphics[width=0.8\textwidth]{../Conclusion/fig/fig2b}
		\scriptsize $J_{ijk}^{lmn} = \left< ijk  \left| f_{12}\,f_{13} \right| lmn \right>$
	\end{minipage}
	\begin{minipage}[b]{0.3\textwidth}
		\includegraphics[width=0.8\textwidth]{../Conclusion/fig/fig2c}
		\scriptsize $J_{ijk}^{lmn} \overset{\text{RG}} \approx \sum_{\mu\nu} S_{il,\mu\nu} S_{jm,\mu} S_{kn,\nu}$
	\end{minipage}
	%
	\begin{minipage}[b]{0.3\textwidth}
		\includegraphics[width=0.8\textwidth]{../Conclusion/fig/fig4a}
		\scriptsize $L_{ijk}^{lmn} \overset{\text{RI}} \approx \sum_{\mu \nu \lambda} F_{i\mu}^{j\nu} G_{\nu m}^{k \lambda} F_{\mu l}^{\lambda n}$
	\end{minipage}
	\begin{minipage}[b]{0.3\textwidth}
		\includegraphics[width=0.8\textwidth]{../Conclusion/fig/fig4b}
		\scriptsize $L_{ijk}^{lmn} = \left<ijk \left| f_{12}\,r_{23}^{-1}\,f_{13} \right| lmn \right>$
	\end{minipage}
	\begin{minipage}[b]{0.3\textwidth}
		\includegraphics[width=0.8\textwidth]{../Conclusion/fig/fig4c}
		\scriptsize $L_{ijk}^{lmn} \overset{\text{RG}} \approx \sum_{\mu \nu} S_{il,\mu\nu} G_{jm,\mu}^{kn,\nu}$
	\end{minipage}
	\caption{RI (left) vs RG (right) for the three-electron integrals $I_{ijk}^{lmn}$, $J_{ijk}^{lmn}$ and $L_{ijk}^{lmn} $.}
	\label{fig:RIvsRG}
\end{figure*}

%************************************************************************
\paragraph{A zero-variance hybrid MP2 method}
%************************************************************************
The stochastic MP2 algorithm developed by Hirata and coworkers \cite{Willow12, Willow14, Johnson16, Gruneis17} has been shown to be particularly promising due to its low computational scaling and its independence towards the correlation factor \cite{Johnson17}.
We propose to investigate two potential improvements of So Hirata's stochastic MP2 algorithm.
First, following the philosophy of the multireference perturbation theory algorithm we have published recently \cite{PT2}, we would like to introduce a small deterministic orbital window.
It would allow to catch the largest fraction of the MP2 correlation energy using a small number of MOs around the Fermi level.\footnote{This idea is somewhat related to the semi-stochastic FCIQMC algorithm developed by Umrigar and coworkers \cite{Petruzielo12b}.}
This would significantly reduce the statistical error of the stochastic part because the magnitude of the statistical error in a Monte Carlo calculation is proportional to the magnitude of the actual expectation value one is actually computing.
Second, we would like to propose a zero-variance version of Hirata's stochastic algorithm \cite{Assaraf99}.
Indeed, one of the weak point of his algorithm comes from the choice of the probability distribution function.
Surely, a zero-variance algorithm would significantly decrease the statistical error on the MP2 correlation energy.

%************************************************************************
\paragraph{Chemistry without Coulomb singularity}
%************************************************************************
One of the most annoying feature of the Coulomb operator is its divergence as $\ree \to 0$.
Indeed, the exact wave function must have a well-defined cusp at electron coalescences so that the infinite Coulomb interaction is exactly cancelled by an opposite divergence in the kinetic term.
Removing such a divergence would have one very important consequence: accelerating the rate of convergence of the energy with respect to the one-electron basis set (see, for example, Ref.~\cite{Franck15}).

A well-known procedure to remove the Coulomb singularity is to use a range-separated operator \cite{Savin96}:
\begin{equation}
	\frac{1}{\ree} = \frac{\erfc(\omega \ree)}{\ree} + \frac{\erf(\omega \ree)}{\ree},
\end{equation}
where $\erf(x)$ is the error function and $\erfc(x) = 1 - \erf(x)$ its complementary version.
The parameter $\omega$ controls the separation range.
For $\omega = 0$, the long-range interaction vanishes while, for $\omega \to \infty$, the short range disappears
In range-separated methods, two different methods are usually used for the short-range and long-range parts.
For example, in range-separated DFT, one usually used DFT for the short-range interaction and a wave function method (such as MP2) to model the long-range part \cite{Toulouse04}.

Here, we propose something slightly different.
What if we approximate the Coulomb operator by its long-range component only, i.e.
\begin{equation}
        \frac{1}{\ree} \approx \frac{\erf(\omega \ree)}{\ree},
\end{equation}
with $\omega$ large enough to be chemically meaningful?
The variational energy would definitely be altered by such a choice \cite{Prendergast01}. 
But what about the nodes? At the end of the day, the nodes are the only things which matters in DMC!
Are the nodes obtained with this attenuated Coulomb operator worse than the ones obtained with the genuine, singular Coulomb operator?
As stated above, the singularity of the Coulomb operator gives birth to Kato's cusp. 
However, for a same-spin electron pair, the node in the wave function at $\ree = 0$ is produced by the antisymmetry of the wave function (Pauli exclusion principle).
For an opposite-spin electron pair, Kato taught us that the wave function is non-zero at $\ree = 0$. 
In other words, there can't be a node!
Therefore, one could argue that removing the Coulomb singulary would have a marginal effect on the nodal surface of the electronic wave function.
This is what we would like to investigate in the future.

Amother possiblity would be to create a local or non-local pseudopotential for the electron-electron interaction.

