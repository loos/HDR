%****************************************************************
\section{Universality of correlation effects}
%****************************************************************
Understanding and calculating the electronic correlation energy is one of the most important and difficult problems in theoretical chemistry.
In this pursuit, the study of high-density correlation energy using perturbation theory has been particularly profitable, shedding light on the physically relevant density regime and providing {\em exact} results for key systems, such as the uniform electron gas \cite{GellMann57} and two-electron systems \cite{BetheSalpeter}.
The former is the cornerstone of the most popular density functional paradigm (the local-density approximation) in solid-state physics \cite{ParrYang}; the latter provide important test cases in the development of new explicitly-correlated methods \cite{Kutzelnigg85,Nakashima07} for electronic structure calculations \cite{Helgaker}.

%****************************************************************
\subsection{High-density correlation energy}
%****************************************************************
The high-density correlation energy of the helium-like ions is obtained by expanding both the exact \cite{Hylleraas30} and HF \cite{Linderberg61} energies as series in $1/Z$, yielding
\begin{subequations}
\begin{align}
\label{eq:Eex}
	E(Z,D,V) 
	& = E^{(0)}(D,V) Z^2
	+ E^{(1)}(D,V) Z
	+ E^{(2)}(D,V) 
	+ \frac{E^{(3)}(D,V)}{Z} 
	+ \ldots,
	\\
\label{eq:EHF}
	E_{\rm HF}(Z,D,V) 
	& = E^{(0)}(D,V) Z^2
	+ E^{(1)}(D,V) Z
	+ E_{\rm HF}^{(2)}(D,V)
	+ \frac{E_{\rm HF}^{(3)}(D,V)}{Z}
	+ \ldots,
\end{align}
\end{subequations}
where $Z$ is the nuclear charge, $D$ is the dimension of the space and $V$ is the external Coulomb potential.
Equations \eqref{eq:Eex} and \eqref{eq:EHF} share the same zeroth- and first-order energies because the exact and the HF treatment have the same zeroth-order Hamiltonian.
Thus, in the high-density (large-$Z$) limit, the correlation energy is
\begin{equation}
\begin{split}
	\Ec^{(2)}(D,V)
	& =  \lim_{Z\to\infty} \Ec(Z,D,V) 
	\\
	& = \lim_{Z\to\infty} \qty[ E(Z,D,V)-E_{\rm HF}(Z,D,V) ]
	\\
	& = E^{(2)}(D,V) - E_{\rm HF}^{(2)}(D,V).
\end{split}
\end{equation}
Despite intensive study \cite{Schwartz62, Baker90}, the coefficient $E^{(2)}(D,V)$ has not yet been reported in closed form.
However, the accurate numerical estimate 
\begin{equation}
\label{eq:E2-He-3D}
	E^{(2)} = -0.157\;666\;429\;469\;14
\end{equation}
has been determined for the important $D=3$ case \cite{Baker90}.
Combining \eqref{eq:E2-He-3D} with the exact result \cite{Linderberg61}
\begin{equation}
\label{eq:E2HF-He-3D}
	E_{\rm HF}^{(2)} = \frac{9}{32} \ln \frac{3}{4} - \frac{13}{432}
\end{equation}
yields a value of 
\begin{equation}
	\Ec^{(2)} = -0.046\;663\;253\;999\;48
\end{equation}
for the helium-like ions in a three-dimensional space.

In the large-$D$ limit, the quantum world reduces to a simpler semi-classical one \cite{Yaffe82} and problems that defy solution in $D=3$ sometimes become exactly solvable.
In favorable cases, such solutions provide useful insight into the $D=3$ case and this strategy has been successfully applied in many fields of physics \cite{Witten80, Yaffe83}.
Indeed, just as one learns something about interacting systems by studying non-interacting ones and introducing the interaction perturbatively, one learns something about $D = 3$ by studying the large-$D$ case and introducing dimension-reduction perturbatively.

Singularity analysis \cite{Doren87} reveals that the energies of two-electron atoms possess first- and second-order poles at $D=1$, and that the Kato cusp \cite{Kato57, Morgan93} is directly responsible for the second-order pole.
In our previous work \cite{EcLimit09, Ballium10}, we have expanded the correlation energy as a series in $1/(D-1)$ but, although this is formally correct if summed to infinite order, such expansions falsely imply higher-order poles at $D=1$.
For this reason, we now follow Herschbach and Goodson \cite{Herschbach86, Goodson87}, and expand both the exact and HF energies as series in $1/D$.
Although various possibilities exist for this dimensional expansion \cite{Doren86, Doren87, Goodson92, Goodson93}, it is convenient to write
\begin{subequations}
\begin{align}
	E^{(2)}(D,V)
	& = \frac{E^{(2,0)}(V)}{D^2}
	+ \frac{E^{(2,1)}(V)}{D^3}
	+ \ldots,
	\label{eq:E2DV}
	\\
	E_{\rm HF}^{(2)}(D,V)
	& = \frac{E_{\rm HF}^{(2,0)}(V)}{D^2}
	+ \frac{E_{\rm HF}^{(2,1)}(V)}{D^3}
	+ \ldots,
	\label{eq:EHF2DV}
	\\
	\Ec^{(2)}(D,V)
	& = \frac{\Ec^{(2,0)}(V)}{D^2}
	+ \frac{\Ec^{(2,1)}(V)}{D^3}
	+ \ldots,
	\label{eq:Ec2DV}
\end{align}
\end{subequations}
where
\begin{subequations}
\begin{align}
	\Ec^{(2,0)}(V) 
	& = E^{(2,0)}(V) - E_{\rm HF}^{(2,0)}(V),
	\\
	\Ec^{(2,1)}(V) 
	& = E^{(2,1)}(V) - E_{\rm HF}^{(2,1)}(V).
\end{align}
\end{subequations}

Such double expansions of the correlation energy were originally introduced for the helium-like ions, and have lead to accurate estimations of correlation \cite{Loeser87a, Loeser87b} and atomic energies \cite{Loeser87c, Kais93} {\em via} interpolation and renormalisation techniques.
Equations \eqref{eq:E2DV}, \eqref{eq:EHF2DV} and \eqref{eq:Ec2DV} apply equally to the $^1S$ ground state of any two-electron system confined by a spherical potential $V(r)$.

%****************************************************************
\subsection{The conjecture}
%****************************************************************
For the helium-like ions, it is known \cite{Mlodinow81, Herschbach86, Goodson87} that
\begin{align}
	\Ec^{(2,0)}(V) & = - \frac{1}{8},
	&
	\Ec^{(2,1)}(V) & = - \frac{163}{384},
\end{align}
and we have recently found \cite{EcLimit09} that $\Ec^{(2,0)}(V)$ takes the same value in hookium (two electrons in a parabolic well \cite{Kestner62, White70, Kais89, Taut93}), spherium (two electrons on a sphere \cite{Ezra82, Seidl07b, TEOAS09, QuasiExact09}) and ballium (two electrons in a ball \cite{Thompson02, Thompson05, Ballium10}).  
In contrast, we found that $\Ec^{(2,1)}(V)$ is $V$-dependent.
The fact that the term $\Ec^{(2,0)}$ is invariant, while $\Ec^{(2,1)}$ varies with the confinement potential allowed us to explain why the high-density correlation energy of the previous two-electron systems are similar, but not identical, for $D=3$ \cite{EcLimit09, Ballium10}.
On this basis, we conjectured \cite{EcLimit09} that
\begin{equation}
\label{eq:conjecture}
        \Ec^{(2)}(D,V) \sim - \frac{1}{8D^2} - \frac{C(V)}{D^3}
\end{equation}
holds for \emph{any} spherical confining potential, where the coefficient $C(V)$ varies slowly with $V(r)$.

%	BEGIN TABLE 1
\begin{table}
\centering
\caption{
\label{tab:Ec}
$E^{(2,0)}$, $E_{\rm HF}^{(2,0)}$, $\Ec^{(2,0)}$ and $\Ec^{(2,1)}$ coefficients for various systems and $v(r) = 1$.}
\begin{tabular}{lrcccc}
\hline
System		&	$m$		&	$-E^{(2,0)}$		&	$-E_{\rm HF}^{(2,0)}$	&	$-\Ec^{(2,0)}$		&	$-\Ec^{(2,1)}$	\\
\hline                                                                          
Helium		&	$-1$		&		$5/8$		&	$1/2$			&	$1/8$			&	$0.424479$	\\
Airium		&	$1$		&		$7/24$		&	$1/6$			&	$1/8$			&	$0.412767$	\\
Hookium		&	$2$		&		$1/4$		&	$1/8$			&	$1/8$			&	$0.433594$	\\
Quartium	&	$4$		&		$5/24$		&	$1/12$			&	$1/8$			&	$0.465028$	\\
Sextium		&	$6$		&		$3/16$		&	$1/16$			&	$1/8$			&	$0.486771$	\\
Ballium		&	$\infty$	&		$1/8$		&	$0$			&	$1/8$			&	$0.664063$	\\
\hline
\end{tabular}
\end{table}
%	END TABLE 1

%****************************************************************
\subsection{The proof}
%****************************************************************
Here, we will summarise our proof of the conjecture \eqref{eq:conjecture}.  
More details can be found in Ref.~\cite{EcProof10}.  
We prove that $\Ec^{(2,0)}$ is universal, and that, for large $D$, the high-density correlation energy of the $^1S$ ground state of two electrons is given by \eqref{eq:conjecture} for any confining potential of the form
\begin{equation}
\label{eq:V-proof}
	V(r) = \text{sgn}(m) r^m v(r),
\end{equation}
where $v(r)$ possesses a Maclaurin series expansion
\begin{equation}
	v(r)	= v_0 + v_1 r + v_2 \frac{r^2}{2} + \ldots.
\end{equation}

After transforming both the dependent and independent variables \cite{EcProof10}, the Schr\"odinger equation can be brought to the simple form
\begin{equation}
\label{eq:Hersch-trans}
	\qty( \frac{1}{\Lambda} \Hat{\mathcal{T}} 
	+ \Hat{\mathcal{U}} 
	+ \Hat{\mathcal{V}} 
	+ \frac{1}{Z} \Hat{\mathcal{W}} ) \Phi_D
	= \mathcal{E}_D \Phi_D,
\end{equation}
in which, for $S$ states, the kinetic, centrifugal, external potential and Coulomb operators are, respectively,
\begin{gather}
	-2 \Hat{\mathcal{T}} = 
	\qty( \frac{\partial^2}{\partial r_1^2} + \frac{\partial^2}{\partial r_2^2} )
	+ \qty( \frac{1}{r_1^2} + \frac{1}{r_1^2} ) 
	\qty( \frac{\partial^2}{\partial \theta^2} + \frac{1}{4} ),
	\\
	\Hat{\mathcal{U}} = 
	\frac{1}{2 \sin^2 \theta} 
	\qty( \frac{1}{r_1^2} + \frac{1}{r_1^2} ),
	\\
	\Hat{\mathcal{V}} =  
	V(r_1) + V(r_2),
	\\
	\Hat{\mathcal{W}} =
	\frac{1}{\sqrt{r_1^2 + r_2^2 -2 r_1 r_2 \cos \theta}},
\end{gather}
and the dimensional perturbation parameter is
\begin{equation}
	\Lambda = \frac{(D-2)(D-4)}{4}.
\end{equation}
In this form, double perturbation theory can be used to expand the energy in terms of both $1/Z$ and $1/\Lambda$.

For $D=\infty$, the kinetic term vanishes and the electrons settle into a fixed (``Lewis'') structure \cite{Herschbach86} that minimises the effective potential
\begin{equation}
\label{eq:X}
	\Hat{\mathcal{X}} 
	= \Hat{\mathcal{U}} + \Hat{\mathcal{V}} 
	+ \frac{1}{Z} \Hat{\mathcal{W}}.
\end{equation}
The minimization conditions are
\begin{gather}
	\frac{\partial \Hat{\mathcal{X}}(r_1,r_2,\theta)}{\partial r_1} =
	\frac{\partial \Hat{\mathcal{X}}(r_1,r_2,\theta)}{\partial r_2} = 0,
	\label{eq:dW-r}
	\\
	\frac{\partial \Hat{\mathcal{X}}(r_1,r_2,\theta)}{\partial \theta} = 0,
	\label{eq:dW-theta}
\end{gather}
and the stability condition implies $m > -2$.  Assuming that the two electrons are equivalent, the resulting exact energy is
\begin{equation}
	\label{eq:Einf}
	\mathcal{E}_{\infty} 
	= \Hat{\mathcal{X}} (r_{\infty},r_{\infty},\theta_{\infty}).
\end{equation}
It is easy to show that
\begin{gather}
	r_{\infty} = \alpha + \frac{\alpha^2}{m+2} 
	\qty(\frac{1}{2\sqrt{2}} - \Lambda \frac{m+1}{m} \frac{v_1}{v_0} ) \frac{1}{Z}
	+ \ldots,
	\label{eq:r-eq}
	\\
	\cos \theta_{\infty} = - \frac{\alpha}{4\sqrt{2}} \frac{1}{Z}
	+ \ldots,
	\label{eq:tetha-eq}
\end{gather}
where $\alpha^{-(m+2)} = \text{sgn}(m) m v_0$.

For the HF treatment, we have $\theta_{\infty}^{\rm HF} = \pi/2$.  
Indeed, the HF wave function itself is independent of $\theta$, and the only $\theta$ dependence comes from the $D$-dimensional Jacobian, which becomes a Dirac delta function centred at $\pi/2$ as $D\to\infty$.  
Solving \eqref{eq:dW-r}, one finds that $r_{\infty}^{\rm HF}$ and $r_{\infty}$ are equal to second-order in $1/Z$.  
Thus, in the large-$D$ limit, the HF energy is
\begin{equation}
	\label{eq:EinfHF}
	\mathcal{E}_{\infty}^{\rm HF}
	= \Hat{\mathcal{X}} \qty(r_{\infty}^{\rm HF},r_{\infty}^{\rm HF},\frac{\pi}{2}),
\end{equation}
and correlation effects originate entirely from the fact that $\theta_\infty$ is slightly greater than $\pi/2$ for finite $Z$.

Expanding \eqref{eq:Einf} and \eqref{eq:EinfHF} in terms of $Z$ and $D$ yields
\begin{align}
	E^{(2,0)}(V) 
	& = - \frac{1}{8} - \frac{1}{2(m+2)},
	\\
	E_{\rm HF}^{(2,0)}(V) 
	& = - \frac{1}{2(m+2)},
	\label{eqEHF20}
\end{align}
thus showing that both $E^{(2,0)}$ and $E_{\rm HF}^{(2,0)}$ depend on the leading power $m$ of the external potential but not on $v(r)$.

Subtracting these energies yields
\begin{equation}
\label{eq:Ec00}
	\Ec^{(2,0)}(V) = - \frac{1}{8},
\end{equation}
and this completes the proof that, in the high-density limit, the leading coefficient $\Ec^{(2,0)}$ of the large-$D$ expansion of the correlation energy is universal, {\em i.e.} it does not depend on the external potential $V(r)$.

The result \eqref{eq:Ec00} is related to the cusp condition \cite{Kato57, Morgan93, Pan03}
\begin{equation}
\label{eq:cusp}
	\left. \pdv{\Psi_D}{\ree}\right|_{\ree=0} 
	= \frac{1}{D-1} \Psi_D(\ree=0),
\end{equation}
which arises from the cancellation of the Coulomb operator singularity by the $D$-dependent angular part of the kinetic operator \cite{Helgaker}.

The $E^{(2,1)}$ and $\EHF^{(2,1)}$ coefficients can be found by considering the Langmuir vibrations of the electrons around their equilibrium positions \cite{Herschbach86, Goodson87}.
The general expressions depend on $v_0$ and $v_1$, but are not reported here.  
However, for $v(r)=1$, which includes many of the most common external potentials, we find
\begin{equation}
	\Ec^{(2,1)}(V) = - \frac{85}{128} - \frac{9/32}{(m+2)^{3/2}}
	+ \frac{1/2}{(m+2)^{1/2}} + \frac{1/16}{(m+2)^{1/2}+2},
\end{equation}
showing that $\Ec^{(2,1)}$, unlike $\Ec^{(2,0)}$, is potential-dependent.  
Numerical values of $\Ec^{(2,1)}$ are reported in Table \ref{tab:Ec} for various systems.


