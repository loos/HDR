	By solving the Schr\"odinger equation, one can predict some of the chemistry and most of the physics of a given system. 
However, although this statement is true and philosophically important, it was realised many years ago that, for more than \textit{one} electron, it is usually far too difficult from the mathematical point of view to solve this mighty equation. 
As Dirac pointed out,
\begin{quote}
	\textit{``The aim of science is to make difficult things understandable in a simpler way.''}
\end{quote}

Consequently, it is essential to develop simple approximations that are accurate enough to have chemical and physical usefulness. 
To do this, quantum chemists and physicists have developed a variety of simple models that, despite their simplicity, contain the key physics of more complicated and realistic systems. 
A few examples are:
\begin{itemize}
	\item the Born-Oppenheimer model: the motions of nuclei and electrons are independent;
	\item the orbital model:  electrons occupy orbitals and move independently of one another;
	\item the local density model: the molecular electron density is built as an assembly of uniform electron gas densities.
\end{itemize}
Nowadays, all of these models are routinely applied in theoretical and/or computational studies. 
Spherical models are another example. 
One of the most popular starting points for modelling complex real life phenomenon by a highly simplified scientific model is the spherical geometry, and the most famous illustration of this is probably the so-called \textit{spherical cow} (Fig.~\ref{fig:cow}). 
While appearing completely nonsensical to most people outside the scientific area, these spherical models can be extremely powerful for understanding, explaining and even predicting physical and chemical phenomena in a wide range of disciplines of physics and chemistry. 
Besides, they offer unparalleled mathematical simplicity, while retaining much of the key physics. 

An explicit example is the spherical model introduced by Haldane \cite{Haldane83} to explain the fractional quantum Hall effect (FQHE), for which Laughlin, St\"ormer and Tsui received the Nobel prize in physics. 
This geometry has been instrumental in establishing the validity of the FQHE theory, and provides the cleanest proof for many properties. 
In this chapter, we will show that the spherical geometry can be also useful to better understand the structure of the exact electronic wave function.

Almost ten years ago, following this idea, we undertook a comprehensive study of two electrons on the surface of a sphere of radius $R$ \cite{TEOAS09, Concentric10, Hook10}.
We used quantum chemistry electronic structure models ranging from HF to state-of-the-art explicitly correlated treatments, the last of which leads to near-exact wave functions and energies. 
This helped us to understand not only the complicated relative motion of electrons, but also the errors inherent to each method.  
It eventually led to the important discovery that the system composed of two electrons restricted to the surface of a $D$-sphere (where $D$ is the dimensionality of the surface of the sphere) is exactly solvable for a countable infinite set of values of $R$ \cite{QuasiExact09, ExSpherium10, QR12, ConcentricExact14}.
In other words, it means that the \textit{exact} solution of the Schr\"odinger equation can be obtained for certain ``magic'' values of the radius of the sphere \cite{QuasiExact09}.
This discovery propelled the two-electrons-on-a-sphere model (subsequently named spherium), into the exclusive family of exactly solvable two-electron models. 
Moreover, after an exhaustive study of the ground state of two electrons confined by various external potentials \cite{EcLimit09, Ballium10}, we noticed that the correlation energy is weakly dependent on the external potential, and we conjectured that the behaviour of the two-electron correlation energy, in the limit of large dimension, is \textit{universal}! 
The rigorous proof of this conjecture has been published in Ref.~\cite{EcProof10}. 
In particular, we showed that the limiting correlation energy at high-density in helium and spherium are amazingly similar \cite{Frontiers10}.
However, while the closed-form expression of the limiting correlation energy has never been found for helium, the value for spherium is quite simple to obtain. 
This shows the superiority of the spherical geometry approach and that it can be used in quantum chemistry to provide robust and trustworthy models for understanding, studying and explaining ``real world'' chemical systems. 

In this chapter, we will summarise some of our key discoveries.

%%% FIG 1 %%%
\begin{figure}
	\centering
	\includegraphics[width=0.2\textwidth]{../Chapter2/fig/sphericalcow}
	\caption{\label{fig:cow} A spherical cow.}
\end{figure}
%%%	%%%

