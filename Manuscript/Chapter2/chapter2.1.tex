%****************************************************************
\section{Quasi-exactly solvable models}
%****************************************************************
Quantum mechanical models for which it is possible to solve explicitly for a finite portion of the energy spectrum are said to be quasi-exactly solvable \cite{Ushveridze}.
They have ongoing value and are useful both for illuminating more complicated systems and for testing and developing theoretical approaches, such as DFT \cite{Hohenberg64, Kohn65, ParrYang} and explicitly-correlated methods \cite{Kutzelnigg85, Kutzelnigg91, Henderson04, Bokhan08}.
One of the most famous quasi-solvable model is the Hooke's law atom which consists of a pair of electrons, repelling Coulombically but trapped in a harmonic external potential with force constant $k$.  
This system was first considered nearly 50 years ago by Kestner and Sinanoglu \cite{Kestner62}, solved analytically in 1989 for one particular $k$ value \cite{Kais89}, and later for a countably infinite set of $k$ values \cite{Taut93}.

A related system consists of two electrons trapped on the surface of a sphere of radius $R$.  
This has been used by Berry and collaborators \cite{Ezra82, Ezra83, Ojha87, Hinde90} to understand both weakly and strongly correlated systems and to suggest an ``alternating'' version of Hund's rule \cite{Warner85}.
Seidl utilised this system to develop new correlation functionals \cite{Seidl00a, Seidl00b} within the adiabatic connection in DFT \cite{Seidl07b}.
As mentioned earlier, we will use the term ``spherium'' to describe this two-electron system.

In Ref.~\cite{TEOAS09}, we examined various schemes and described a method for obtaining near-exact estimates of the $^1S$ ground state energy of spherium for any given $R$.
Because the corresponding HF energies are also known exactly, this is now one of the most complete theoretical models for understanding electron correlation effects.

In this section, we consider the $D$-dimensional generalisation of this system in which the two electrons are trapped on a $D$-sphere of radius $R$.  
We adopt the convention that a $D$-sphere is the surface of a ($D+1$)-dimensional ball.  
Here, we show that the Schr\"odinger equation for the $^1S$ and the $^3P$ states can be solved exactly for a countably infinite set of $R$ values and that the resulting wave functions are polynomials in the interelectronic distance $\ree = \abs{\br_1-\br_2}$.  
Other spin and angular momentum states can be addressed in the same way using the ansatz derived by Breit \cite{Breit30} and we will discuss these excited states later in this section \cite{ExSpherium10}.
We have also published dedicated studies of the 1D system (that we dubbed ringium) \cite{QR12, Ringium13, NatRing16}.
The case of two concentric spheres has also been considered in two separate publications \cite{Concentric10, ConcentricExact14}, as well as the extension to excitonic wave functions \cite{Exciton12}.
Finally, the nodal structures of these systems has been investigated in collaboration with Dario Bressanini \cite{Nodes15}.

%****************************************************************
\subsection{Singlet ground state}
%****************************************************************
The electronic Hamiltonian is
\begin{equation}
	\HOp = - \frac{\nabla_1^2}{2} - \frac{\nabla_2^2}{2} + \frac{1}{\ree},
\end{equation}
and because each electron moves on a $D$-sphere, it is natural to adopt hyperspherical coordinates \cite{Louck60}.

For $^1S$ states, it can be then shown \cite{TEOAS09} that the wave function $S(\ree)$ satisfies the Schr{\"o}dinger equation
\begin{equation} 
\label{eq:S-singlet}
	\qty[ \frac{\ree^2}{4R^2} - 1 ] \dv[2]{S(\ree)}{\ree} + \qty[ \frac{(2D-1)\ree}{4R^2} - \frac{D-1}{\ree} ] \dv{S(\ree)}{\ree} + \frac{S(\ree)}{\ree} = E\,S(\ree).
\end{equation}
By introducing the dimensionless variable $x = \ree/2R$, this becomes a Heun equation \cite{Ronveaux} with singular points at $x = -1, 0, +1$.  
Based on our previous work \cite{TEOAS09} and the known solutions of the Heun equation \cite{Polyanin}, we seek wave functions of the form
\begin{equation} 
\label{eq:S_series}
	S(\ree) = \sum_{k=0}^\infty s_k\,\ree^k,
\end{equation}
and substitution into \eqref{eq:S-singlet} yields the recurrence relation
\begin{equation} 
\label{eq:recurrence-singlet}
	s_{k+2} = \frac{ s_{k+1} + \qty[ k(k+2D-2) \frac{1}{4R^2} - E ] s_k }{(k+2)(k+D)},
\end{equation}
with the starting values
\begin{equation}
	\{s_0,s_1\} =	\begin{cases}
				\qty{0,1},		&	D = 1,
				\\
				\qty{1,1/(D-1)},	&	D \ge 2.
			\end{cases}
\end{equation}
Thus, the Kato cusp conditions \cite{Kato57} are
\begin{align}
\label{eq:cusp-circle}
	S(0) & = 0,	
	&	
	\frac{S''(0)}{S'(0)} & = 1,
\end{align}
for electrons on a ring ($D=1$), i.e.~ringium, and
\begin{equation} 
\label{eq:S-cusp}
	\frac{S'(0)}{S(0)} = \frac{1}{D-1},
\end{equation}
in higher dimensions. 
We note that the ``normal'' Kato value of 1/2 arises for $D=3$ --- a system we called glomium as the name of a 3-sphere is a glome --- suggesting that this may the most appropriate model for atomic or molecular systems.  
We will return to this point below. 

The wave function \eqref{eq:S_series} reduces to the polynomial
\begin{equation}
	S_{n,m}(\ree) = \sum_{k=0}^n s_k\,\ree^k,
\end{equation}
(where $m$ the number of roots between $0$ and $2R$) if, and only if, $s_{n+1} = s_{n+2} = 0$.  
Thus, the energy $E_{n,m}$ is a root of the polynomial equation $s_{n+1} = 0$ (where $\deg s_{n+1} = \lfloor (n+1)/2 \rfloor$) and the corresponding radius $R_{n,m}$ is found from \eqref{eq:recurrence-singlet} which yields
\begin{equation} 
\label{eq:E_S}
	R_{n,m}^2 E_{n,m} = \frac{n}{2}\qty(\frac{n}{2}+D-1).
\end{equation}
$S_{n,m}(\ree)$ is the exact wave function of the $m$-th excited state of $^1S$ symmetry for the radius $R_{n,m}$.

%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}
\centering
\caption{\label{tab:lowest} Radius $R$, energy $E$ and wave function $S(\ree)$ or $T(\ree)$ of the first $^1S$ and $^3P$ polynomial solutions for two electrons on a $D$-sphere}
\centering
\begin{tabular}{ccccc}
\hline
		State		&	$D$	&	$2R$		&	$E$	&	$S(\ree)$ or $T(\ree)$	\\
\hline
\mr{4}{*}{$^1S$}		&	1	&	$\sqrt{6}$	&	2/3	&	$\ree(1 + \ree/2)$		\\
				&	2	&	$\sqrt{3}$	&	1	&	$1 + \ree$			\\
				&	3	&	$\sqrt{10}$	&	1/2	&	$1 + \ree/2$			\\
				&	4	&	$\sqrt{21}$	&	1/3	&	$1 + \ree/3$			\\
\hline
\mr{4}{*}{$^3P$}		&	1	&	$\sqrt{6}$	&	1/2	&	$1 + \ree/2$			\\
				&	2	&	$\sqrt{15}$ 	&	1/3	&	$1 + \ree/3$			\\
				&	3	&	$\sqrt{28}$	&	1/4	&	$1 + \ree/4$			\\
				&	4	&	$\sqrt{45}$	&	1/5	&	$1 + \ree/5$			\\
\hline
\end{tabular}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%

%****************************************************************
\subsection{Triplet excited state}
%****************************************************************
If we write the $^3P$ state wave function as \cite{Breit30}
\begin{equation}
	^3\Psi = (\cos \theta_1 - \cos \theta_2)\,T(\ree),
\end{equation}
where $\theta_1$ and $\theta_2$ are the $D$-th hyperspherical angles of the two electrons \cite{Louck60}, the symmetric part satisfies the Schr{\"o}dinger equation
\begin{equation} 
\label{eq:S-triplet}
	\qty[ \frac{\ree^2}{4R^2} - 1 ] \dv[2]{T(\ree)}{\ree} + \qty[ \frac{(2D+1)\ree}{4R^2} - \frac{D+1}{\ree} ] \dv{T(\ree)}{\ree} + \frac{T(\ree)}{\ree} = E\,T(\ree),
\end{equation}
and the antisymmetric part provides an additional kinetic energy contribution $D/(2R^2)$.

Substituting the power series expansion
\begin{equation} 
\label{eq:T_series}
	T(\ree) = \sum_{k=0}^\infty t_k\,\ree^k
\end{equation}
into \eqref{eq:S-triplet} yields the recurrence relation
\begin{equation} 
\label{eq:recurrence-triplet}
	t_{k+2} = \frac{ t_{k+1} + \qty[ k(k+2D) \frac{1}{4R^2} - E ] t_k }{(k+2)(k+D+2)},
\end{equation}
with the starting values
\begin{equation}
	\qty{t_0,t_1} = \qty{1, 1/(D+1)},
\end{equation}
yielding the cusp condition
\begin{equation} 
\label{eq:T-cusp}
	\frac{T'(0)}{T(0)} = \frac{1}{D+1}.
\end{equation}

The wave function \eqref{eq:T_series} reduces to the polynomial
\begin{equation}
	T_{n,m}(\ree) = \sum_{k=0}^n t_k\,\ree^k,
\end{equation}
when the energy $E_{n,m}$ is a root of $t_{n+1} = 0$ and the corresponding radius $R_{n,m}$ is found from \eqref{eq:recurrence-triplet} which yields
\begin{equation} 
\label{eq:E_T}
	R_{n,m}^2 E_{n,m} = \frac{n}{2}\qty(\frac{n}{2}+D).
\end{equation}
$T_{n,m}(\ree)$ is the exact wave function of the $m$-th excited state of $^3P$ symmetry for the radius $R_{n,m}$.

It is illuminating to begin by examining the simplest $^1S$ and $^3P$ polynomial solutions.  
Except in the $D=1$ case, the first $^1S$ solution has
\begin{align}
	R_{1,0} & = \sqrt{\frac{(2D-1)(2D-2)}{8}},	&	E_{1,0} & = \frac{1}{D-1},
\end{align}
and the first $^3P$ solution has
\begin{align}
	R_{1,0} & = \sqrt{\frac{(2D+1)(2D+2)}{8}},	&	E_{1,0} & = \frac{1}{D+1}.
\end{align}
These are tabulated for $D = 1, 2, 3, 4$, together with the associated wave functions, in Table \ref{tab:lowest}.

In the ringium ($D=1$) case (\textit{i.e.}~two electrons on a ring), the first singlet and triplet solutions have $E_{2,0} = 2/3$ and $E_{1,0} = 1/2$, respectively, for the same value of the radius ($\sqrt{6}/2 \approx 1.2247$). 
The corresponding wave functions are related by $S_{2,0} = \ree\,T_{1,0}$.  
Unlike $T_{1,0}$, the singlet wavefunction $S_{2,0}$ vanishes at $\ree = 0$, and exhibits a second-order cusp condition, as shown in \eqref{eq:cusp-circle}.

For spherium ($D=2$ case), we know from our previous work \cite{TEOAS09} that the HF energy of the lowest $^1S$ state is $\EHF = 1/R$.  
It follows that the exact correlation energy for $R = \sqrt{3}/2$ is $\Ec = 1-2/\sqrt{3} \approx -0.1547$ which is much larger than the limiting correlation energies of the helium-like ions ($-0.0467$) \cite{Baker90} or Hooke's law atoms ($-0.0497$) \cite{Gill05}.  
This confirms our view that electron correlation on the surface of a sphere is qualitatively different from that in three-dimensional physical space.

For glomium ($D=3$ case), in contrast, possesses the same singlet and triplet cusp conditions --- Eqs.~\eqref{eq:S-cusp} and \eqref{eq:T-cusp} --- as those for electrons moving in three-dimensional physical space.  
Indeed, the wave functions in Table \ref{tab:lowest}
\begin{align}
	S_{1,0}(\ree) & = 1 + \ree/2,	&	(R & = \sqrt{5/2}),	
	\\
	T_{1,0}(\ree) & = 1 + \ree/4,	&	(R & = \sqrt{7}),
\end{align}
have precisely the form of the ansatz used in Kutzelnigg's increasingly popular R12 methods \cite{Kutzelnigg85, Kutzelnigg91}.
Moreover, it can be shown \cite{EcLimit09} that, as $R \to 0$, the correlation energy $\Ec$ approaches $-0.0476$, which nestles nicely between the corresponding values for the helium-like ions ($-0.0467$) \cite{Baker90} and the Hooke's law atom ($-0.0497$) \cite{Gill05}.
Again, this suggests that the $D=3$ model (``electrons on a glome'') bears more similarity to common physical systems than the $D=2$ model (``electrons on a sphere'').
We will investigate this observation further in the next section.

%For fixed $D$, the radii increase with $n$ but decrease with $m$, and the energies behave in exactly the opposite way.  
%As $R$ (or equivalently $n$) increases, the electrons tend to localize on opposite sides of the sphere, a phenomenon known as Wigner crystallization \cite{Wigner34} which has also been observed in other systems \cite{Thompson04a, Thompson04b, Taut93}.
%As a result, for large $R$, the ground state energies of both the singlet and triplet state approach $1/(2R)$.  
%Analogous behavior is observed when $D \to \infty$ \cite{Yaffe82, Goodson87}.

%****************************************************************
\subsection{Other electronic states}
%****************************************************************
As shown in Ref.~\cite{ExSpherium10}, one can determine exact wave functions for other electronic states, but not all of them.
These states are inter-connected by subtile interdimensional degeneracies (see Table \ref{tab:summary}) using the transformation $(D,L) \rightarrow (D+2,L-1)$, where $L$ is the total angular momentum of the state.
We refer the interested readers to Refs.~\cite{Herrick75a, Herrick75b, ExSpherium10, eee15, Nodes15} for more details.

The energies of the $S$, $P$ and $D$ states ($m=0$) for glomium are plotted in Fig.~\ref{fig:ES} (the quasi-exact solutions are indicated by markers), while density plots of spherium ($n=1$ and $m=0$) are represented on Fig. \ref{fig:ES-on-sphere}.

%	FIGURE 1
\begin{figure}
	\centering
	\includegraphics[width=0.6\textwidth]{../Chapter2/fig/ES}
	\caption{
	\label{fig:ES}
	Energy of the $S$, $P$ and $D$ states of glomium
	(${}^1S^{\rm e} < {}^3P^{\rm o} \leq {}^1P^{\rm o} < {}^3P^{\rm e} < {}^3D^{\rm e} < {}^1D^{\rm o} \leq {}^3D^{\rm o}$).
	The quasi-exact solutions are shown by the markers.}
\end{figure}

%	FIGURE 2
\begin{figure}
	\centering
	\includegraphics[width=0.6\textwidth]{../Chapter2/fig/ES-on-sphere}
	\caption{
	\label{fig:ES-on-sphere}
	Density plots of the $S$, $P$ and $D$ states of spherium.
	The squares of the wave functions when one electron is fixed at the north pole are represented.
	The radii are $\sqrt{3}/2$, $\sqrt{15}/2$, $\sqrt{5}/2$, $\sqrt{21}/2$, $\sqrt{21}/2$, $3\sqrt{5}/2$ and $3\sqrt{3}/2$ for the ${}^1S^{\rm e}$, ${}^3P^{\rm o}$, ${}^1P^{\rm o}$, ${}^3P^{\rm e}$, ${}^3D^{\rm e}$, ${}^1D^{\rm o}$ and ${}^3D^{\rm o}$ states, respectively.}
\end{figure}

%****************************************************************
\subsection{Natural/unnatural parity}
%****************************************************************
In attempting to explain Hund's rules \cite{Hund25} and the ``alternating'' rule \cite{Russel27, Condon} (see also Refs.~\cite{Boyd84, Warner85}), Morgan and Kutzelnigg \cite{Kutzelnigg92, Morgan93, Kutzelnigg96} have proposed that the two-electron atomic states be classified as:

\begin{quote}
	\textit{A two-electron state, composed of one-electron spatial orbitals with individual parities $(-1)^{\ell_1}$ and $(-1)^{\ell_1}$ and hence with overall parities $(-1)^{\ell_1+\ell_2}$, is said to have natural parity if its parity is $(-1)^L$. [\ldots] If the parity of the two-electron state is $-(-1)^{L}$, the state is said to be of unnatural parity.}
\end{quote}

After introducing spin, three classes emerge.  
In a three-dimensional space, the states with a cusp value of $1/2$ are known as the {\em natural parity singlet states} \cite{Kato51,Kato57}, those with a cusp value of $1/4$ are the {\em natural and unnatural parity triplet states} \cite{Pack66}, and those with a cusp value of $1/6$, are the {\em unnatural parity singlet states} \cite{Kutzelnigg92}.

%In the previous section, we have observed that the $^1S^{\rm e}$ ground state and the first excited $^3P^{\rm o}$ state of glomium possess the same singlet ($1/2$) and triplet ($1/4$) cusp conditions as those for electrons moving in three-dimensional physical space and we have therefore argued that glomium may be the most appropriate model for studying ``real'' atomic or molecular systems.  
%As mentioned previously, this is supported by the similarity of the correlation energy of glomium to that in other two-electron systems.  

Most of the higher angular momentum states of glomium, possess the ``normal'' cusp values of $1/2$ and $1/4$.  
However, the unnatural $^1D^{\rm o}$ and $^1F^{\rm e}$ states have the cusp value of $1/6$.

%****************************************************************
\subsection{First-order cusp condition}
%****************************************************************
The wave function, radius and energy of the lowest states are given by
\begin{align}
	\Psi_{1,0} (\ree) 	& = 1 + \gamma\,\ree,	
				&
	R_{1,0}^2 		& = \frac{\delta}{4\gamma}, 	
				&
	E_{1,0} 		& = \gamma, 
\end{align}
which are closely related to the Kato cusp condition \cite{Kato57}
\begin{equation} 
\label{eq:Kato}
        \frac{\Psi^{\prime}(0)}{\Psi(0)} = \gamma.
\end{equation}

We now generalise the Morgan-Kutzelnigg classification \cite{Morgan93} to a $D$-dimensional space.  
Writing the interparticle wave function as
\begin{equation}
\label{eq:Psi-1}
	\Psi(\ree) = 1 + \frac{\ree}{2\kappa+D-1} + O(\ree^2),
\end{equation}
we have 
\begin{equation}
\label{eq:classification}
\begin{split}
	\kappa = 0,	& 	\text{ for natural parity singlet states,}	
	\\
	\kappa = 1,	& 	\text{ for triplet states,}	
	\\
	\kappa = 2,	& 	\text{ for unnatural parity singlet states.}
\end{split}
\end{equation}
The labels for states of two electrons on a $D$-sphere are given in Table \ref{tab:summary}.

%	TABLE 1
\begin{table*}
\centering
\caption{
\label{tab:summary}
Ground state and excited states of two electrons on a $D$-sphere}
\begin{tabular}{ccccccc}
\hline
State		&	Configuration	&	$\delta$	&	$\gamma^{-1}$		&	$\Lambda$	& $\kappa$		&	Degeneracy	\\
\hline                                                                                                                                                                                                                                                          
$^1S^{\rm e}$	&	$s^2$	&	$2D-1$	&	$D-1$	&	0		&	0	&	$^3P^{\rm e}$	\\
\hline
$^3P^{\rm o}$	&	$sp$	&	$2D+1$  &	$D+1$	&	$D/2$		&	1	&	$^1D^{\rm o}$	\\
$^1P^{\rm o}$	&	$sp$	&	$2D+1$	&       $D-1$	&	$D/2$		&	0	&	$^3D^{\rm o}$	\\
$^3P^{\rm e}$	&	$p^2$	&	$2D+3$	&	$D+1$	&	$D$		&	1	&			\\
\hline
$^3D^{\rm e}$	&	$sd$	&	$2D+3$	&	$D+1$	&	$D+1$		&	1	&	$^1F^{\rm e}$	\\
$^1D^{\rm o}$	&	$pd$	&	$2D+5$	&	$D+3$	&	$3D/2+1$	&	2	&			\\
$^3D^{\rm o}$	&	$pd$	&	$2D+5$	&	$D+1$	&	$3D/2+1$	&	1	&			\\
\hline
$^1F^{\rm e}$ 	&	$pf$	&	$2D+7$	&	$D+3$	&	$2D+3$		&	2	&			\\
\hline
\end{tabular}
\end{table*}

%****************************************************************
\subsection{Second-order cusp condition}
%****************************************************************
The second solution is associated with 
\begin{gather}
	\Psi_{2,0} (\ree) = \Psi_{1,0} (\ree) 
	+ \frac{\gamma ^2 (\delta +2)}{2 \gamma  (\delta +2)+4 \delta +6} \ree^2,	
	\\
	R_{2,0}^2 = \frac{(\gamma+2)(\delta+2)-1}{2\gamma},			
	\\
	E_{2,0} = \frac{\gamma(\delta+1)}{(\gamma+2)(\delta+2)-1}.
\end{gather}
For two electrons on a $D$-sphere, the second-order cusp condition is
\begin{equation}
\label{eq:Psi2}
	\frac{\Psi^{\prime\prime}(0)}{\Psi(0)} 
	= \frac{1}{2D} \left( \frac{1}{D-1} - E \right).
\end{equation}
Following \eqref{eq:Psi2}, the classification \eqref{eq:classification} can be extended to the second-order coalescence condition, where the wave function (correct up to second-order in $u$) is
\begin{equation}
	\Psi(\ree) 
	= 1 + \frac{\ree}{2\kappa+D-1} 
	+ \frac{\ree^2}{2(2\kappa+D)} \left( \frac{1}{2\kappa+D-1} - E \right) + O(\ree^3).
\end{equation}
Thus, we have, for $D = 3$,
\begin{equation}
	\frac{\Psi^{\prime\prime}(0)}{\Psi(0)} =
	\begin{cases}
	\frac{1}{6}  \qty( \frac{1}{2} - E ),	&	\text{ for } \kappa = 0,	
	\\
	\frac{1}{10} \qty( \frac{1}{4} - E ),	&	\text{ for } \kappa = 1,	
	\\
	\frac{1}{14} \qty( \frac{1}{6} - E ),	&	\text{ for } \kappa = 2.
	\end{cases}
\end{equation}
For the natural parity singlet states ($\kappa=0$), the second-order cusp condition of glomium is precisely the second-order coalescence condition derived by Tew \cite{Tew08}, reiterating that glomium is an appropriate model for normal physical systems.

