%%%%%%%%%%%%%%%%%%%%%%%%%
\section{
\label{sec:RR}
Recurrence relations}
%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{
\label{sec:VRR}
Vertical recurrence relations}
%%%%%%%%%%%%%%%%%%%%%%%%%
Following Obara and Saika \cite{OS1, OS2}, vertical RRs (VRRs) are obtained by differentiation of Eq.~\eqref{eq:Fund0m} with respect to the centers coordinates \cite{Ahlrichs06, 3ERI1}.
For the integrals considered in this chapter, one can show that
\begin{equation}
\begin{split}
\label{eq:VRR}
	\sexpval{\cdots \ba_i^+ \cdots}^{m} 
	& = 
	\qty( \bZ_i\bA_i - \DOp_i Y_0 ) \sexpval{\cdots \ba_i \cdots}^{m} 
%	\\
%	& 
	- \qty( \DOp_i Y_1 - \DOp_i Y_0 ) \sexpval{\cdots \ba_i \cdots}^{m+1}
	\\
	& + \sum_{j=1}^n \ba_j \Bigg\{ \qty( \frac{\delta_{ij}}{2\zeta_i} - \DOp_{ij} Y_0 )  \sexpval{\cdots \ba_j^- \cdots}^{m} 
%	\\
%	&
	 - \qty( \DOp_{ij} Y_1 - \DOp_{ij} Y_0 ) \sexpval{\cdots \ba_j^- \cdots}^{m+1} \Bigg\},
	\end{split}
\end{equation}
where $\delta_{ij}$ is the Kronecker delta \cite{NISTbook},
\begin{align}
	\DOp_i & = \frac{\nabla_{A_i}}{2\alpha_i},
	&
	\DOp_{ij} & = \DOp_i \DOp_j,
\end{align}
and
\begin{subequations}
\begin{align}
	\DOp_i Y_u & = \Tr(\bm{\Delta}_u \cdot \DOp_i \bY^2),
	&
	(\DOp_i \bY^2)_{kl} & = \kappa_{ikl} (\bY)_{kl},
	\\
	\DOp_{ij} Y_u & = \Tr(\bm{\Delta}_u \cdot \DOp_{ij} \bY^2),
	&
	(\DOp_{ij} \bY^2)_{kl} & = \frac{\kappa_{ikl}\kappa_{jkl}}{2},
\end{align}
\end{subequations}
with
\begin{align}
	\eps_{ij} & = 
	\begin{cases}
		1,	&	\text{if $i \le j$},
		\\
		0,	&	\text{otherwise},
	\end{cases}
	&
	\kappa_{ijk} & = \frac{\eps_{ij} \delta_{ki} - \delta_{ij} \eps_{ki} }{\zeta_i}.
\end{align}
One can easily derive VRRs for other three- and four-electron operators following the simple rules given in Fig.~\ref{fig:tree}.
The number of terms for each of these VRRs is reported in Table \ref{tab:RRcount} for various two-, three- and four-electron operators.

Note that for a pure GG operator, we have $m = 0$ and $Y_1 = Y_0$.
Therefore, Eq.~\eqref{eq:VRR} reduces to a simpler expression:
\begin{equation}
\label{eq:VRR-pure}
%\begin{split}
	\sexpval{\cdots \ba_i^+ \cdots}
%	& 
	= \qty( \bZ_i\bA_i - \DOp_i Y_0 ) \sexpval{\cdots \ba_i \cdots} 
%	\\
%	& 
+ \sum_{j=1}^n \ba_j \qty( \frac{\delta_{ij}}{2\zeta_i} - \DOp_{ij} Y_0 )  \sexpval{\cdots \ba_j^- \cdots}.
%\end{split}
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{
\label{sec:TRR}
Transfer recurrence relations}
%%%%%%%%%%%%%%%%%%%%%%%%%
Transfer RRs (TRRs) redistribute angular momentum between centres referring to different electrons \cite{3ERI1}.
Using the translational invariance, one can derive 
\begin{equation}
\label{eq:TRR}
%\begin{split}
	\sexpval{\cdots \ba_i^+ \cdots}
%	& 
	= \sum_{j=1}^n \frac{\ba_j}{2\zeta_i } \sexpval{\cdots \ba_j^- \cdots}
	- \sum_{j \neq i}^n \frac{\zeta_j}{\zeta_i } \sexpval{\cdots \ba_j^+ \cdots}
%	\\
%	& 
	- \frac{\sum_{j=1}^n \beta_j \bA_j \bB_j}{\zeta_i } \sexpval{\cdots \ba_j \cdots}.
%\end{split}
\end{equation}
Note that Eq.~\eqref{eq:TRR} can only be used to build up angular momentum on the last center.
Moreover, to increase the momentum by one unit on this last center, one must increase the momentum by the same amount on all the other centres (as evidenced by the second term in the right-hand side of \eqref{eq:TRR}).
Therefore, the TRR is computationally expensive for three- and four-electron integrals due to the large number of centres (see below).
As mentioned by Ahlrichs \cite{Ahlrichs06}, the TRR can be beneficial for very high angular momentum two-electron integral classes.

%===========================
\subsection{
\label{sec:HRR}
Horizontal recurrence relations}
%===========================
The so-called horizontal RRs (HRRs) enable to shift momentum between centres over the same electronic coordinate \cite{3ERI1}:
\begin{equation}
\label{eq:HRR}
%\begin{split}
	\braket{\cdots \ba_i \cdots}{\cdots \bb_i^+ \cdots}
%	& 
	= \braket{\cdots \ba_i^+ \cdots}{\cdots \bb_i \cdots}
%	\\
%	&
	 + \bA_i \bB_i \braket{\cdots \ba_i \cdots}{\cdots \bb_i \cdots}.
%\end{split}
\end{equation}
Note that HRRs can be applied to contracted integrals because they are independent of the contraction coefficients and exponents.

%%% TABLE 1 %%%
\begin{table*}
\footnotesize
\centering
\caption{
\label{tab:RRcount}
Number of intermediates required to compute various integral classes for two-, three- and four-electron operators.
The path generating the minimum number of intermediates is highlighted in bold.
The number of terms in the RRs and the associated incremental center are also reported.}
\begin{tabular}{llllllcccc}
\hline
Integral                        &        type           &       operator                                &       path            & number                & centers               &       \mc{4}{c}{integral class}       \\
                                                                                                                                                        \cline{7-10}
                                &                       &                                               &                       & of terms              &       &       $\sexpval{p \ldots p}$  &       $\sexpval{d \ldots d}$  &       $\sexpval{f \ldots f}$  &       $\sexpval{g \ldots g}$  \\
\hline
two-electron            &       chain           &       $G_{12}$                                &       \bf VV          &       \bf (2,3)               &       ($\bA_2$,$\bA_1$)                                       &       \bf 3           &       \bf 6   &       \bf 10  &       \bf 15          \\
                                &                       &                                               &       VT                      &       (2,4)                   &       ($\bA_2$,$\bA_1$)                                       &       4               &       9       &       16      &       25              \\
                                &                       &       $C_{12}$                                &       \bf VV          &       \bf (4,6)               &       ($\bA_2$,$\bA_1$)                                       &       \bf 4           &       \bf 13  &       \bf 25  &       \bf 48  \\
                                &                       &                                               &       VT                      &       (4,4)                   &       ($\bA_2$,$\bA_1$)                                       &       7               &       19      &       37      &       61              \\
three-electron          &       chain           &       $G_{13}G_{23}$          &       \bf VVV         &       \bf (2,3,4)             &       ($\bA_3$,$\bA_2$,$\bA_1$)                       &       \bf 5           &       \bf 13  &       \bf 26  &       \bf 45  \\
                                &                       &                                               &       VVT                     &       (2,3,6)         &       ($\bA_3$,$\bA_1$,$\bA_2$)                       &       8               &       25      &       56      &       105             \\
                                &                       &       $C_{12}G_{23}$          &       VVV                     &       (4,5,7)         &       ($\bA_3$,$\bA_1$,$\bA_2$)                       &       11              &       39      &       96      &       195             \\
                                &                       &                                               &       \bf VVV         &       \bf (4,6,6)             &       ($\bA_3$,$\bA_2$,$\bA_1$)                       &       \bf 10  &       \bf 39  &       \bf 96  &       \bf 196 \\
                                &                       &                                               &       VVT                     &       (4,5,6)         &       ($\bA_3$,$\bA_1$,$\bA_2$)                       &       16              &       66      &       173     &       359             \\
                                &                       &                                               &       VVT                     &       (4,6,6)         &       ($\bA_3$,$\bA_2$,$\bA_1$)                       &       15              &       65      &       171     &       357             \\
                                &       cyclic  &       $C_{12}G_{13}G_{23}$    &       \bf VVV         &       \bf (4,6,8)             &       ($\bA_3$,$\bA_2$,$\bA_1$)                       &       \bf 12  &       \bf 46  &       \bf 119 &       \bf 250 \\
                                &                       &                                               &       VVT                     &       (4,6,6)         &       ($\bA_3$,$\bA_2$,$\bA_1$)                       &       16              &       66      &       173     &       359             \\
four-electron           &       chain           &       $C_{12}G_{14}G_{23}$    &       VVVV            &       (4,5,7,8)               &       ($\bA_4$,$\bA_3$,$\bA_2$,$\bA_1$)       &       21              &       108     &       344     &       847             \\                                              &                       &                                               &       \bf VVVV                &       \bf (4,6,6,8)   &       ($\bA_4$,$\bA_1$,$\bA_3$,$\bA_2$)       &       \bf 19  &       \bf 88  &       \bf 260 &       \bf 607 \\
                                &                       &                                               &       VVVT            &       (4,5,7,8)               &       ($\bA_4$,$\bA_3$,$\bA_2$,$\bA_1$)       &       33              &       208     &       736     &       1,926   \\
                                &                       &                                               &       VVVT            &       (4,6,6,8)               &       ($\bA_4$,$\bA_1$,$\bA_3$,$\bA_2$)       &       33              &       204     &       716     &       1,866   \\
                                &                       &       $C_{12}G_{13}G_{34}$    &       VVVV            &       (4,6,6,9)               &       ($\bA_4$,$\bA_3$,$\bA_2$,$\bA_1$)       &       22              &       113     &       360     &       888             \\
                                &                       &                                               &       \bf VVVV                &       \bf (4,6,8,7)   &       ($\bA_4$,$\bA_3$,$\bA_1$,$\bA_2$)       &       \bf 20  &       \bf 98  &       \bf 302 &       \bf 726 \\
                                &                       &                                               &       VVVT            &       (4,6,6,8)               &       ($\bA_4$,$\bA_3$,$\bA_2$,$\bA_1$)       &       33              &       204     &       716     &       1,866   \\
                                &                       &                                               &       VVVT            &       (4,6,8,8)               &       ($\bA_4$,$\bA_3$,$\bA_1$,$\bA_2$)       &       34              &       214     &       756     &       1,976   \\
                                &       trident         &       $C_{12}G_{13} G_{14}$   &       VVVV            &       (4,6,6,9)               &       ($\bA_4$,$\bA_3$,$\bA_2$,$\bA_1$)       &       22              &       113     &       360     &       888             \\
                                &                       &                                               &       \bf VVVV                &       \bf (4,6,8,7)   &       ($\bA_4$,$\bA_3$,$\bA_1$,$\bA_2$)       &       \bf 20  &       \bf 98  &       \bf 302 &       \bf 726 \\
                                &                       &                                               &       VVVT            &       (4,6,6,8)               &       ($\bA_4$,$\bA_3$,$\bA_2$,$\bA_1$)       &       33              &       204     &       716     &       1,866   \\
                                &                       &                                               &       VVVT            &       (4,6,8,8)               &       ($\bA_4$,$\bA_3$,$\bA_1$,$\bA_2$)       &       34              &       214     &       756     &       1,976   \\
	\hline
\end{tabular}
\end{table*}
%%%
