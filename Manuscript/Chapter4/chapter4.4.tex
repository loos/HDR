%==============
\section{
\label{sec:UB}
Upper bounds}
%==============
In this section, instead of reporting the mathematical expressions of UBs (which can be found in Ref.~\cite{IntF12}), we present general concepts about UBs for three- and four-electron integrals.
We refer the interested reader to Refs.~\cite{Gill94a, GG16} for additional information about UBs.

As mentioned earlier in this chapter, computing $\order{N^{6}}$ or $\order{N^{8}}$ integrals in a large system would be extremely challenging, but it turns out that the number of \emph{significant} three- and four-electron integrals is, at worst, $\order{N^{3}}$ and $\order{N^{4}}$, respectively.  
Moreover, if the correlation factor is a short-range operator (as in modern F12 methods \cite{Persson96, Persson97, May04, Tenno04a, Tenno04b, Tew05, May05}), it can be shown that the number drops to only $\order{N^{2}}$.  
However, to exploit this fully, one must devise rigorous UBs and then use these to avoid computing vast numbers of negligible integrals.  
If this can be achieved, it may enable large-scale F12 calculations without the need to introduce RI approximations.

From a general point of view, an effective UB should be: 
\begin{itemize}
	\item simple, i.e.~much cheaper than the true integral;
	\item strong, i.e.~as close as possible to the true integral;
	\item scaling-consistent, i.e.~$N_\text{sig} = \order{N_\text{UB}}$, where $N_\text{UB}$ is the number estimated by the UB.
\end{itemize}
Many two-electron integral UBs are known \cite{Power74, Ahlrichs74, Haser89, Horn91, Gill94a} but few satisfy all three requirements.  
Moreover, to the best of our knowledge, the only three-electron integral UB that has been proposed is a simple application of the Cauchy-Schwartz inequality \cite{DahleThesis}.

To construct UBs, we will depend heavily on the absolute value inequality
\begin{equation}
	\abs{ \int \phi(\br) d\br } \le  \int \abs{\phi(\br)} d\br,
\end{equation}
and the H\"{o}lder inequality \cite{NISTbook}
\begin{equation}
	\abs{ \int \phi_{1}(\br)\phi_{2}(\br) d\br } \le  \qty[ \int \abs{\phi_{1}(\br)}^{p} d\br ]^{1/p}  \qty[ \int \abs{\phi_{2}(\br)}^{q} d\br ]^{1/q} , 
\end{equation}
where $p^{-1}+q^{-1}=1$ and $p,q > 1$.  H\"older yields the Cauchy-Schwartz inequality \cite{NISTbook} if one chooses $p=q=2$.
Note, however, that we eschewed bounds descending from the Cauchy-Schwartz inequality because they are usually weaker than ours \cite{Gill94a} and, for three-electron integrals, they are usually not simple.

%-----------------------------------------------------
\subsection{\label{sec:ib} Integral bounds}
%-----------------------------------------------------
An integral bound is a number that bounds \emph{a particular integral}.  For example, the Cauchy-Schwartz inequality yields the well-known \cite{Ahlrichs74} two-electron integral bound
\begin{equation}
	\abs{\sbraket{\ba_1\ba_2}{\bb_1\bb_2}} \le \sbraket{\ba_1\ba_1}{\bb_1\bb_1}^{1/2} \sbraket{\ba_2\ba_2}{\bb_2\bb_2}^{1/2} .
\end{equation}
If one has pre-computed and stored $\order{N^{2}}$ Cauchy-Schwartz factors $\sbraket{\ba_1\ba_1}{\bb_1\bb_1}$, these yield cheap upper bounds on $\order{N^{4}}$ two-electron integrals. 

However, despite their attractive features, integral bounds are poorly suited to modern hardware and software.  
Bounding every integral before deciding whether or not to compute its exact value places logical branches within inner loops and leads to slow code.  
Moreover, using bounds to eliminate a few integrals from a class is incompatible with recursive methods for integral generation.  
This leads naturally to a class strategy.

%----------------------------------------------------
\subsection{\label{sec:cb} Class bounds}
%----------------------------------------------------
A class bound is a number that bounds \emph{all the integrals in a class}.  
These are particularly effective for large classes because, if the class bound is below $\tau$, a large number of integrals can be skipped on the basis of one test.  
Spherical bounding Gaussians (SBGs) $\sba$, as introduced in Ref.~\cite{GG16, IntF12}, lead naturally to class bounds, for example,
\begin{equation} \label{eq:cb1}
	\abs{\sbraket{a_1a_2}{b_1b_2}} \le \sbraket{\sba_{1} \sba_{2}}{\sbb_{1} \sbb_{2}}.
\end{equation}

Non-separable class bounds, such as \eqref{eq:cb1}, involve quantities that have the same asymptotic scaling as the integrals.  
Such bounds are therefore always scaling-consistent.

Separable class bounds, such as the Cauchy-Schwartz bound derived from \eqref{eq:cb1}
\begin{equation} \label{eq:hb1}
	\left| \sbraket{a_1a_2}{b_1b_2}  \right| \le \sbraket{\sba_1\sba_1}{\sbb_1\sbb_1}^{1/2} \sbraket{\sba_2\sba_2}{\sbb_2\sbb_2}^{1/2}
\end{equation}
involve factors that may scale differently from the integrals themselves.  
Such bounds may not be scaling-consistent.

A specific example may be helpful.  The number of significant two-electron integrals over long-range and short-range operators is $\order{N^{2}}$ and $\order{N}$, respectively.  
However, the separable bound \eqref{eq:hb1} predicts $\order{N^{2}}$ in both cases and is therefore scaling-inconsistent for short-range operators.  
In situations when one cannot find a scaling-consistent separable bound, one should use a non-separable bound.

Note that bounding an entire class of integrals with a single UB is a particularly desirable feature, especially when dealing with three- or four-electron integrals where the size of a class can be extremely large. 
For example, the simple $\sbraket{ppp}{ppp}$ and $\sbraket{pppp}{pppp}$ classes are made of 729 and 4,096 integrals!

%-----------------------------------------------------------------
\subsection{\label{sec:sb} Shell-$m$tuplet bounds}
%-----------------------------------------------------------------
A shell-$m$tuplet bound $B_m$ relies only on shell-$m$tuplet information, where $m$ is the shell multiplicity: shell-pair ($m=2$), shell-quartet ($m=4$), shell-sextet ($m=6$), etc. 
If $B_m > \tau$, it indicates that the shell-$m$tuplet is significant, i.e.~it could yield significant integrals.
A shell-$m$tuplet bound is a class bound that depends only on the operator, the basis set and the shell multiplicity $m$, independent of the maximum shell multiplicity $n$ of the integrals.  
It is also scaling-consistent at its specific shell-multiplet level.

%-----------------------------------------------------------------
\subsection{Screening algorithm}
%-----------------------------------------------------------------
Our screening algorithms are based on primitive, $[B_{m}]$, and contracted, $\expval{B_{m}}$, shell-$m$tuplet bounds.   
Figure \ref{fig:scheme1} is a schematic representation of the overall screening scheme for contracted four-electron integrals.
First, we use a primitive shell-pair bound $\sexpval{B_2}$ to create a list of significant primitive shell-pairs.
For a given contracted shell-pair, if at least one of its primitive shell-pairs has survived, a contracted shell-pair bound $\expval{B_2}$ is used to decide whether or not this contracted shell-pair is worth keeping.
The second step consists in using a shell-quartet bound $\expval{B_4}$ to create a list of significant contracted shell-quartets by pairing the contracted shell-pairs with themselves.
Then, we combine the significant shell-quartets and shell-pairs, and a shell-sextet bound $\expval{B_6}$ identifies the significant contracted shell-sextets.  
Finally, the shell-sextets are paired with the shell-pairs. 
If the resulting shell-octet quantity is found to be significant, the contracted integral class $\braket{a_1a_2a_3a_4}{b_1b_2b_3b_4}$ must be computed via RRs, as discussed in the next section.
Following this strategy, the size of any shell-$m$tuplet list is, at worst, quadratic in a large system.

During the shell-pair screening, either a contracted or a primitive path is followed depending on the degree of contraction of the integral class $K_\text{tot}=\prod_{i=1}^{n} K_{a_{i}}K_{b_{i}}$.
If $K_\text{tot}>1$, the contracted path is enforced, otherwise the primitive path is followed.
This enables to adopt the more effective primitive bounds for primitive integral classes which are usually associated with medium and high angular momentum PGFs and, therefore, are more expensive to evaluate via RRs \cite{IntF12}.
The scheme for primitive four-electron integrals differs only by the use of primitive bounds instead of contracted ones.
The three-electron integrals screening scheme can be easily deduced from Fig.~\ref{fig:scheme1}.

%%% FIGURE 2 %%%
\begin{figure*}
	\centering
        \includegraphics[width=\linewidth]{../Chapter4/fig/fig2}
\caption{
\label{fig:scheme1}
Schematic representation of the screening algorithm used to compute contracted four-electron integrals.}
\end{figure*}
%%%     %%%
