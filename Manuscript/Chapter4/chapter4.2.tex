%==================
\section{Generalities}
%==================
%==================
\subsection{
\label{sec:gaussian}
Gaussian functions}
%==================
A primitive Gaussian function (PGF) is specified by an orbital exponent $\alpha$, a center $\bA=(A_x,A_y,A_z)$, and angular momentum $\ba=(a_{x},a_{y},a_{z})$:
\begin{equation}
\label{eq:def1}
	\varphi_{\ba}^{\bA}(\br)  	
	= (x-A_{x})^{a_{x}} (y-A_{y})^{a_{y}} (z-A_{z})^{a_{z}} e^{-\alpha \abs{\br-\bA}^2}.
\end{equation}
A contracted Gaussian function (CGF) is defined as a sum of PGFs
\begin{equation}
\label{eq:def1b}
	\psi_{\ba}^{\bA}(\br)	
	=\sum_{k=1}^{K_a} D_{ak} (x-A_{x})^{a_{x}} (y-A_{y})^{a_{y}} (z-A_{z})^{a_{z}} e^{-\alpha_k \abs{\br-\bA}^2},
\end{equation}
where $K_a$ is the degree of contraction and the $D_{ak}$ are contraction coefficients.
A CGF-pair 
\begin{equation} 
\label{eq:CGTO-pair}
	\ket{\ba \bb} \equiv \psi_{\ba}^{\bA}(\br)\psi_{\bb}^{\bB}(\br) = \sum_{i=1}^{K_a} \sum_{j=1}^{K_b} \sket{\ba \bb}_{ij}
\end{equation}
is a two-fold sum of PGF-pairs $\sket{\ba \bb}=\varphi_{\ba}^{\bA}(\br) \varphi_{\bb}^{\bB}(\br)$.

A primitive shell $\sket{a}$ is a set of PGFs sharing the same total angular momentum $a$, exponent $\alpha$ and center $\bA$. 
Similarly, a contracted shell $\ket{a}$ is a set of CGFs sharing the same PGFs and total angular momentum. 
A contracted shell-pair is the set of CGF-pairs obtained by the tensor product $\ket{a b}=\ket{a} \otimes \ket{b}$.
Similarly, a primitive shell-pair $\sket{a b}=\sket{a} \otimes \sket{b}$ is the set of PGF-pairs.
Finally, primitive and contracted shell-quartets, -sextets and -octets are obtained in an analogous way.
For example, $\sket{a_1 b_1 a_2 b_2} = \sket{a_1 b_1} \otimes \sket{a_2 b_2}$ and $\ket{a_1 a_2 b_1 b_2} = \ket{a_1 b_1} \otimes \ket{a_2 b_2}$.
Note that $\sket{1}$ is a set of three $p$-type PGFs, a $\sket{11} \equiv \sket{pp}$ shell-pair is a set of nine PGF-pairs, and a $\sket{2222} \equiv \sket{dddd}$ shell-quartet is a set of $1,296$ PGF-quartets.

%==================
\subsection{
\label{sec:integrals}
Many-electron integrals}
%==================
Throughout this chapter, we use physicists notations, and we write the integral over a $n$-electron operator $f_{1 \cdots n}$ of CGFs as
%\begin{widetext}
\begin{equation}
\begin{split}
\label{eq:def2}
	\braket{\ba_1 \cdots \ba_n}{\bb_1 \cdots \bb_n}
	& \equiv \mel{\ba_1 \cdots \ba_n}{f_{1\cdots n}}{\bb_1 \cdots \bb_n}
	\\
	& = \idotsint 
	\psi_{\ba_1}^{\bA_1}(  \br_{1}) \cdots \psi_{\ba_n}^{\bA_n}(\br_{n})
	\,f_{1\cdots n}\,
	\psi_{\bb_1}^{\bB_1}(  \br_{1}) \cdots \psi_{\bb_n}^{\bB_n}(\br_{n})
	d \br_{1} \cdots d \br_{n}.
\end{split}
\end{equation}
Additionally, square-bracketed integrals denote integrals over PGFs:
\begin{equation}
\label{eq:def2b}
	\sbraket{\ba_1 \cdots \ba_n}{\bb_1 \cdots \bb_n}
	= \idotsint 
	\varphi_{\ba_1}^{\bA_1}( \br_{1}) \cdots \varphi_{\ba_n}^{\bA_n}(\br_{n})
	\,f_{1 \cdots n}\,
	\varphi_{\bb_1}^{\bB_1}( \br_{1}) \cdots \varphi_{\bb_n}^{\bB_n}(\br_{n})
	d \br_{1} \cdots d \br_{n}.
\end{equation}
The FIs (i.e.~the integral in which all $2n$ basis functions are $s$-type PGFs) is defined as $\sexpval{\bo} \equiv \sbraket{\bo \cdots \bo}{\bo \cdots \bo}$ with $\bo=(0,0,0)$.
The Gaussian product rule reduces it from $2n$ to $n$ centers:
\begin{equation}
\label{eq:def4}
	\sexpval{\bo} = 
	\qty( \prod_{i=1}^n S_{i} )
	\idotsint \varphi_{\bo}^{\bZ_1}(\br_{1}) \cdots \varphi_{\bo}^{\bZ_n}(\br_{n})
	\,f_{1 \cdots n}\,d \br_{1} \cdots d \br_{n},
\end{equation}
%\end{widetext}
where 
\begin{subequations}
\begin{align}
	\zeta_i & = \alpha_i + \beta_i,		
	\\
	\bZ_i & = \frac{\alpha_i \bA_i + \beta_i \bB_i}{\zeta_i},		
	\\ 
	S_{i} & = \exp(-\frac{\alpha_i \beta_i}{\zeta_i} \abs{\bA_i\bB_i}^2),
\end{align}
\end{subequations}
and $\bA_i\bB_i = \bA_i - \bB_i$.
We also define the quantity $\bY_{ij} = \bZ_i - \bZ_j$ which will be used later on.

For conciseness, we will adopt a notation in which missing indices represent $s$-type Gaussians.  
For example, $\sexpval{\ba_2\ba_3}$ is a shorthand for $\sbraket{\bo\ba_2\ba_3\bo}{\bo\bo\bo\bo}$.  
We will also use unbold indices, e.g. $\sbraket{a_1a_2a_3a_4}{b_1b_2b_3b_4}$ to indicate a complete class of integrals from a shell-octet.

%==================
\subsection{
\label{sec:operators}
Three- and four-electron operators}
%==================
In this chapter, we are particularly interested in the ``master'' four-electron operator $C_{12}G_{13}G_{14}G_{23}G_{34}$ (where $C_{12} = \ree^{-1}$ is the Coulomb operator) because the three types of three-electron integrals and the three types of four-electron integrals that can be required in F12 calculations can be easily generated from it (see Fig.~\ref{fig:tree}).
These three types of three-electron integrals are composed by a single type of integrals over the cyclic operator $C_{12}G_{13}G_{23}$, and two types of integrals over the three-electron chain (or 3-chain) operators $C_{12}G_{23}$ and $G_{13}G_{23}$.
F12 calculations may also require three types of four-electron integrals: two types of integrals over the 4-chain operators $C_{12}G_{14}G_{23}$ and $C_{12}G_{13}G_{34}$, as well as one type over the trident operator $C_{12}G_{13}G_{14}$.
Explicitly-correlated methods also requires two-electron integrals. 
However, their computation has been thoroughly studied in the literature \cite{Kutzelnigg91, Klopper92, Persson97, Klopper02, Manby03, Werner03, Klopper04, Tenno04a, Tenno04b, May05, Manby06, Tenno07, Komornicki11, Tenno12a, Tenno12b, Reine12, Kong12, Hattig12}.
Similarly, the nuclear attraction integrals can be easily obtained by taking the large-exponent limit of a $s$-type shell-pair.

Starting with the ``master'' operator $C_{12}G_{13}G_{14}G_{23}G_{34}$, one can easily obtain all the FIs as well as the RRs required to compute three- and four-electron integrals within F12 calculations.
This is illustrated in Fig.~\ref{fig:tree} where we have used a diagrammatic representation of the operators.
The number $N_\text{sig}$ of significant integrals in a large system with $N$ CGFs is also reported.

%%% FIGURE 1 %%%
\begin{figure}
	\centering
	\includegraphics[width=0.6\linewidth]{../Chapter4/fig/fig1}
\caption{
\label{fig:tree}
Diagrammatic representation of the three- and four-electron integrals required in F12 theory.
The number $N_\text{sig}$ of significant integrals in a large system with $N$ CGFs is also reported.}
\end{figure}
%%%	%%%


