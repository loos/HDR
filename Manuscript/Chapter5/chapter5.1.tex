%****************************************************************
\section{Explicitly-correlated FCI methods}
%****************************************************************
One of the most fundamental problem of conventional electronic structure methods is their slow energy convergence with respect to the size of the one-electron basis set.
This problem was already spotted thirty years ago by Kutzelnigg \cite{Kutzelnigg85} who stated that 

\begin{quote}
	\textit{``traditional CI is not really bad, it only has difficulties to represent the wave function at those regions of configuration space where one interelectronic distance $r_{ij}$ approaches zero.''}
\end{quote}

To address this problem he proposed to introduce explicitly the correlation between electrons via the introduction of the interelectronic distance $\ree$ as a basis function \cite{Kutzelnigg91, Termath91, Klopper91a, Klopper91b, Noga94}.
As mentioned in the previous chapter, this yields a prominent improvement of the energy convergence from $O(L^{-3})$ to $O(L^{-7})$ (where $L$ is the maximum angular momentum of the one-electron basis).
This idea was later generalised to more accurate correlation factors $f_{12} \equiv f(\ree)$ \cite{Persson96, Persson97, May04, Tenno04b, Tew05, May05}.
The resulting F12 methods achieve chemical accuracy for small organic molecules with relatively small Gaussian basis sets \cite{Tenno12a, Tenno12b, Hattig12, Kong12}.
For example, as illustrated by Tew and coworkers, one can obtain, at the CCSD(T) level, quintuple-zeta quality correlation energies with a triple-zeta basis \cite{Tew07b}.

Here, following Kutzelnigg's idea, we propose to introduce the explicit correlation between electrons within the CI method via a dressing of the CI matrix \cite{Huron73, Evangelisti83}.
This method, involving effective Hamiltonian theory, has been shown to be successful in other scenarios \cite{Heully92}.
Compared to other explicitly-correlated methods, this dressing strategy has the advantage of introducing the explicit correlation at a low computational cost.
The present explicitly-correlated dressed CI method is completely general and can be applied to any type of truncated, full, or even selected CI methods \cite{Giner13, Scemama13a, Scemama13b, Scemama14, Giner15, Caffarel16}.
However, for the sake of generality, we will discuss here the dressing of the FCI matrix.

%----------------------------------------------------------------
\subsection{Ansatz}
%----------------------------------------------------------------
Inspired by a number of previous research \cite{Shiozaki11}, our electronic wave function ansatz $\ket{\Psi} = \kD + \kF$ is simply written as the sum of a ``conventional'' part
\begin{equation}
\label{eq:D}
	\kD = \sum_{I} c_I \kI
\end{equation}
composed by a linear combination of determinants $\kI$ of coefficients $c_I$ and an ``explicitly-correlated'' part 
\begin{equation}
\label{eq:WF-F12-CIPSI}
	\kF = \sum_{I} t_I \QOp f \kI
\end{equation}
with coefficients $t_I$.
The projector
\begin{equation}
	\QOp = \IdOp - \sum_{I} \dyad{I}
\end{equation}
ensures the orthogonality between $\kD$ and $\kF$, and
\begin{equation}
\label{eq:Ja}
	f = \sum_{i < j} \gamma_{ij} f_{ij}
\end{equation}
is a correlation factor, and
\begin{equation}
	\gamma_{ij} = 
	\begin{cases}
		1/2,	&	\text{for opposite-spin electrons},
		\\
		1/4,	&	\text{for same-spin electrons}.
	\end{cases}
\end{equation}
 
As first shown by Kato \cite{Kato51, Kato57} (and further elaborated by various authors \cite{Pack66, Morgan93}), for small $\ree$, the two-electron correlation factor $f_{12}$ in Eq.~\eqref{eq:Ja} must behave as
\begin{equation}
	f_{12} = \gamma_{12}\,\ree + \order{\ree^2}.
\end{equation}

%----------------------------------------------------------------
\subsection{Dressing}
%----------------------------------------------------------------
Our primary goal is to introduce the explicit correlation between electrons at low computational cost.
Therefore, assuming that $\HOp \ket{\Psi} = E \ket{\Psi}$, one can write, by projection over $\bra{I}$,
\begin{equation}
	c_I \qty[ H_{II} + c_I^{-1} \mel*{I}{\HOp}{F} - E] + \sum_{J \ne I} c_J H_{IJ} = 0,
\end{equation} 
where $H_{IJ} = \mel{I}{\HOp}{J}$.
Hence, we obtain the desired energy by diagonalising the dressed Hamiltonian:
\begin{equation}
\label{eq:DrH}
	\oH_{IJ} = 
		\begin{cases}
			H_{II} + c_I^{-1}\mel*{I}{\HOp}{F},	&	\text{if $I = J$},
			\\
			H_{IJ},										&	\text{otherwise},
		\end{cases}
\end{equation}
with
\begin{equation}
\label{eq:IHF}
	\mel{I}{\HOp}{F} =  \sum_J t_J \qty[ \mel{I}{\HOp f}{J} - \sum_{K}  H_{IK} f_{KJ} ],
\end{equation}
and $f_{IJ} = \mel{I}{f}{J}$.
It is interesting to note that, in an infinite basis, we have $\mel{I}{\HOp}{F} = 0$, which demonstrates that our dressed CI method becomes exact in the limit of a complete one-electron basis.

At this stage, two key comments are in order.
First, as one may have realised, the coefficients $t_I$ are unknown. 
However, they can be set to ensure the correct electron-electron cusp conditions \cite{Tenno04a}.

This yields the following linear system of equations
\begin{equation}
\label{eq:tI}
	\sum_J (\delta_{IJ} + f_{IJ}) t_J = c_I,
\end{equation}
which can be easily solved using standard linear algebra packages.

Second, because Eq.~\eqref{eq:DrH} depends on the CI coefficient $c_I$, one must iterate the diagonalisation process self-consistently until convergence of the desired eigenvalues of the dressed Hamiltonian $\oH$.
At each iteration, we solve Eq.~\eqref{eq:tI} to obtain the coefficients $t_I$ and dress the Hamiltonian [see Eq.~\eqref{eq:DrH}].
In practice, we initially start with a CI vector obtained by the diagonalisation of the undressed Hamiltonian, and convergence is usually reached within few cycles.
This iteration process can be also embedded in the Davidson diagonalisation process, which is also an iterative process.
For pathological cases, a DIIS-like procedure may be employed \cite{Pulay80, Pulay82}.

%%% FIG 1 %%%
\begin{figure}
	\centering
	\includegraphics[width=0.6\linewidth]{../Chapter5/fig/fig1}
	\caption{
	\label{fig:CBS}
	Schematic representation of the various orbital spaces and their notation.
	The arrows represent the three types of excited determinants contributing to the dressing: the pure doubles $\ket*{_{ij}^{\alpha \beta}}$ (green), the mixed doubles $\ket*{_{ij}^{a \beta}}$ (magenta) and the pure singles $\ket*{_{i}^{\alpha}}$ (orange).}
\end{figure}
%%%	%%%

%----------------------------------------------------------------
\subsection{Matrix elements}
%----------------------------------------------------------------
Compared to a conventional CI calculation, new matrix elements are required.
The simplest of them $f_{IJ}$ --- required in Eqs.~\eqref{eq:IHF} and \eqref{eq:tI} --- can be easily computed by applying Slater-Condon rules \cite{Szabo}.
They involve two-electron integrals over the geminal factor $f_{12}$.  
Their computation has been thoroughly studied in the literature in the last thirty years \cite{Kutzelnigg91, Klopper92, Persson97, Klopper02, Manby03, Werner03, Klopper04, Tenno04a, Tenno04b, May05, Manby06, Tenno07, Komornicki11, Reine12, GG16}.
These can be more or less expensive to compute depending on the choice of the correlation factor.

As shown in Eq.~\eqref{eq:IHF}, the present explicitly-correlated CI method also requires matrix elements of the form $\mel{I}{\HOp f}{ J}$.
These are more problematic, as they involve the computation of numerous three-electron integrals over the operator $\ree^{-1}f_{13}$, as well as new two-electron integrals \cite{Kutzelnigg91, Klopper92}.
We have recently developed recurrence relations and efficient upper bounds in order to compute these types of integrals \cite{3ERI1, 3ERI2, 4eRR, IntF12}.

However, we will also explore a different route here.
We propose to compute them using the RI approximation \cite{Kutzelnigg91, Klopper02, Valeev04, Werner07, Hattig12}, which requires a complete basis set (CBS). 
This CBS is built as the union of the orbital basis set (OBS) $\qty{p}$ (divided as occupied $\qty{i}$ and virtual $\qty{a}$ subspaces) augmented by a complementary auxiliary basis set (CABS) $\qty{\alpha}$, such as $ \qty{p} \cap \qty{\alpha} = \varnothing$ and $\braket{p}{\alpha} = 0$ \cite{Klopper02, Valeev04} (see Fig.~\ref{fig:CBS}).

In the CBS, one can write 
\begin{equation}
\label{eq:RI}
	\IdOp = \sum_{A \in \mA} \dyad{A}{A}
\end{equation}
where $\mA$ is the set of all the determinants $\kA$ corresponding to electronic excitations from occupied orbitals $\qty{i}$ to the extended virtual orbital space $\qty{a} \cup \qty{\alpha}$.
Substituting \eqref{eq:RI} into the first term of the right-hand side of Eq.~\eqref{eq:IHF}, one gets
\begin{equation}
\label{eq:IHF-RI}
\begin{split}
	\mel{I}{\HOp}{F} 
	& = \sum_J t_J \qty[ \sum_{A \in \mA} H_{IA} f_{AJ} - \sum_{K \in \mD}  H_{IK} f_{KJ} ]
	\\
	& = \sum_J t_J \sum_{A \in \mC} H_{IA} f_{AJ},
\end{split}
\end{equation}
where $\mD$ is the set of ``conventional'' determinants obtained by excitations from the occupied space $\qty{i}$ to the virtual one $\qty{a}$, and $\mC = \mA \setminus \mD$.
Because $f$ is a two-electron operator, the way to compute efficiently Eq.~\eqref{eq:IHF-RI} is actually very similar to what is done within second-order multireference perturbation theory \cite{PT2}.

Although $\mel{0}{\HOp}{_{i}^{a}} = 0$, note that the Brillouin theorem does not hold in the CABS, i.e.~$\mel{0}{\HOp}{_{i}^{\alpha}} \neq 0$.
Here, we will eschew the generalized Brillouin condition (GBC) which set these to zero \cite{Kutzelnigg91}.
%----------------------------------------------------------------
\subsection{An illustrative example}
%----------------------------------------------------------------
%%% FIG 1 %%%
\begin{figure}
	\centering
        \includegraphics[width=0.45\linewidth]{../Chapter5/fig/glo1}
        \includegraphics[width=0.45\linewidth]{../Chapter5/fig/glo2}
        \caption{
        \label{fig:Glo}
	$\Ec$ for two electrons on a surface of a glome of unit radius as a function of the maximum angular momentum of the basis set $L$.
	For each calculation, the maximum angular momentum of the auxiliary basis is set to $L_\text{RI} = 3 L$.}
\end{figure}
%%%     %%%

To illustrate this method, we have computed the correlation energy of two electrons on a surface of a unit glome --- system we have presented earlier in the memoir --- as a function of the maximum angular momentum of the basis set $L$.
For each calculation, the maximum angular momentum of the auxiliary (or RI) basis is set to $L_\text{RI} = 3 L$.
Note that here, because of the form of the exact wave function presented earlier, we used a correlation factor $f_{12} = r_{12}$.

Various methods have been considered: 
\begin{enumerate}
	\item the conventional FCI method which obviously corresponds to CISD here.
	\item FCI-PT2 in which we compute a second-order Epstein-Nesbet perturbative correction using the auxiliary basis.
	\item the FCI-F12 method where the explicitly-correlated basis function is treated variationally (i.e.~no dressing). 
	\item the dressed FCI-F12 method presented here in which the energy is computed by projection and the dressing term is computed explicitly.
	\item the same dressed FCI-F12 method where the dressing term is computed with the help of the auxiliary basis.
\end{enumerate}

The results are depicted in Fig.~\ref{fig:Glo}.
Few comments are in order:
\begin{itemize}
	\item As expected, the convergence of the conventional FCI method is miserable.
	\item Treating the  explicitly-correlated basis function variationally yields the fastest convergence but it requires the computation of expensive and numerous three- and four-electron integrals.
	\item the dressed FCI-F12 method significantly improves the convergence of the energy. 
	With a relative small number of basis functions, one can reach sub-millihartree accuracy.
	However, the energy is not variational as it is calculated via projection.
	\item the PT2 correction allows to recover a significant fraction of the missing correlation energy.
	However, it does not produce a wave function one can use as a trial wave function for QMC calculations.
	\item the RI approximation induces a large error but still improve upon the conventional FCI method.
	Therefore, we believe that one should try to compute explicitly the three-electron integrals required in the dressed FCI-F12 method.
\end{itemize}

In regards to these results, we believe that the present dressed FCI-F12 method may be an interesting alternative for producing accurate and compact trial wave functions for DMC calculations.
We hope to be able to consider more realistic systems in the near future as well as studying the nodal surfaces of these explicitly-correlated wave functions.

