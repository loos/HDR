%****************************************************************
\section{Motivations}
%****************************************************************
In the last decade, the advent of massively parallel computational platforms and their ever-growing capabilities in terms of number of computing nodes (exceeding now one million!) has unveiled new horizons for studying large quantum systems with genuine physical and/or chemical relevance (see Fig.~\ref{fig:supercomputer}).
It is now widely recognised that there is an imperative need to revisit most of the standard algorithms (or design totally new ones!) to make sure that they take full advantage of these new supercomputer architectures and scale up to an arbitrary number of cores.

Unfortunately, because most of the computational chemistry methods developed so far are mainly based on iterative schemes for solving very large linear systems with stringent I/O and memory constraints, they intrinsically do not scale up, i.e.~they are not able to deliver the result in an elapsed time (restitution time) inversely proportional to the number of cores (in the very large number of cores regime).

A class of methods known to scale up nicely are stochastic approaches.
Taking advantage of this attractive feature, a number of authors has recently proposed to systematically revisit the traditional quantum chemistry methods to make them stochastic in nature.
Note that the fundamental equations are identical, only the way of solving them is different.
Let us mention the stochastic MP2 method of Hirata \cite{Willow12, Willow14}, the FCI-QMC approach of Alavi and coworkers \cite{Booth09, Cleland11}, the stochastic coupled-cluster theory \cite{Thom10}, and the stochastic CASSCF method \cite{Thomas15}.
Quite remarkably, in each case, it is found that the stochastic version is able to surpass the limits of the corresponding deterministic one.
To give a concrete example, the authors of Ref.~\cite{Thomas15} have been able to perform a CASSCF calculation for the coronene molecule in a complete active space of 24 $\pi$ electrons in 24 orbitals, a calculation impossible to perform using standard deterministic CASSCF implementations.
Despite these impressive improvements, major limitations remain...

\begin{figure}
        \centering 
	\includegraphics[width=0.6\textwidth]{../Chapter5/fig/supercomputer}
        \caption{
        \label{fig:supercomputer}
        Concrete applications made possible thanks to the massively parallel nature of the hybrid deterministic/stochastic methods. 
	Biology: study of the rhodopsin protein \cite{Valsson13}; 
	Physics: phase diagram of solid molecular hydrogen at extreme pressures \cite{Drummond15}; 
	Chemistry: absorption spectra in solution \cite{Daday15}.}
\end{figure}

In this final chapter, we present two (unfinished) ideas.
First, we present an explicitly-correlated version of the CI method.
The key idea here is to combine QMC methods and traditional CI-type approaches.
More precisely, we propose to exploit the extremely fast convergence properties of explicitly-correlated methods, and the multi-decade experience of quantum chemistry in building compact albeit accurate wave functions.
The ultimate goal is to design a new highly accurate, black-box QMC method applicable to a wide range of chemical systems and able to run efficiently on current and future massively parallel computers (exascale horizon).
Second, we describe a method for imposing the correct electron-nucleus (e-n) cusp in MOs expanded as a linear combination of (cuspless) Gaussian basis functions. 
Enforcing the e-n cusp in trial wave functions is a important asset in QMC calculations as it significantly reduces the variance of the local energy during the Monte Carlo sampling.

